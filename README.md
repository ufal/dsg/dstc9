# DSTC 9 Task
## Getting started
Start with creating a python 3.7 venv and installing requirements.txt. Python 3.8 is **not supported** by ConvLab-2.
```bash
python -m venv ~/envs/dstc
source ~/envs/dstc/bin/activate
pip install -r requirements.txt
cd ~/source 
git clone git@github.com:ufal/ConvLab-2.git
cd ConvLab-2
pip install -e . --no-deps
python -m spacy download en_core_web_sm
```
Alternatively, you can run `./install.sh` which installs the required files and prepares the submission.

### Data
MultiWOZ 2.1 is a part of the ConvLab repo.

### Training 
To start the training on single CPU node (for testing), run the training with the following command:
```bash
python train.py --no-cuda --gradient-accumulation-steps 4
```

> **_NOTE:_**  For optimal performance at least **four** GPUs are required for training.

To run the training with single GPU:
```bash
python train.py --gradient-accumulation-steps 4
```

To run on single node with multiple GPUs, run the following command:
```bash
python -m torch.distributed.launch --nproc_per_node=NUM_GPUS_YOU_HAVE train.py
```
In this case the expected number of GPUs is four, you may need to adjust *learning_rate* and/or *gradient-accumulation-steps* accordingly.

To run the training on multiple nodes with multiple GPUs, you can use pytorch launch utility <https://pytorch.org/docs/stable/distributed.html#launch-utility>.
Alternatively, consult your job scheduling system. You may need to set the environment variables: `LOCAL_RANK`, `RANK`, `WORLD_SIZE`, `MASTER_PORT`, `MASTER_ADDR`.
In this case, `RANK` is global number of current process across the world and `LOCAL_RANK` is the number of each process running on single node. 
Every node is required to have as many GPUs as there are processes running on single machine.  

## Results
> **_NOTE:_** Every experiment in wandb should have associated with it a **tag** in git.

TODO: put here results of our experiments
Each experiment should be tagged so we can find it later.


## Submission
The submission files are generated automatically by running "./install.sh". This command should be run from a Python 3.7 virtual environment.
