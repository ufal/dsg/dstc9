#!/bin/bash
export WANDB_ANONYMOUS=must
set -e
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$DIR"

# Install convlab 2
mkdir -p ~/source
rm -rf ~/source/convlab2
git clone https://github.com/thu-coai/ConvLab-2.git ~/source/convlab2
pip install --upgrade pip
pip install -e ~/source/convlab2

# Install requirements
python3 -m pip install -r requirements.txt

# Just for evaluation
python3 -m pip install flask
python3 -m spacy download en_core_web_sm

# Pull all required models
while read modelname; do
    if [ -z "$modelname" ]
        then continue
    fi
    python3 -c "import agent; agent.AutoDialogAgent.download_model('$modelname')"
done <end2end/models.txt

# Generate submission files
end2end/generate.sh
