import logging
import argparse
from agent import AutoDialogAgent
from utils import setup_logging


def interact(agent, args):
    history = []
    while True:
        user = input('user:')
        history.append(user)
        belief, database, _, sys, raw = agent([history], lexicalise=True, return_strings=True)[0]
        if args.raw:
            print(f'output:{raw}')
        history.append(sys)
        print(f'sys:{sys}')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('model')
    parser.add_argument('--raw', action='store_true')
    parser.add_argument('--dataset', default='multiwoz-test')
    args = parser.parse_args()
    setup_logging()
    logger = logging.getLogger()

    logger.info('loading model')
    agent = AutoDialogAgent.from_pretrained(args.model)
    logger.info('agent loaded')

    # Interact
    interact(agent, args)
