import data
import pytest
from collections import OrderedDict


dataset_names = ["schemaguided-train", "schemaguided-val", "schemaguided-test"]


@pytest.mark.parametrize('dataset_name', dataset_names)
def test_dataset_loads(dataset_name):
    dataset = data.load_dataset(dataset_name)
    # TODO: uncomment after implementing lazy loading
    # dataset.items = dataset.items[:20]
    dataset = dataset.finish()
    assert isinstance(dataset, data.DialogDataset)
    assert dataset.database is not None
    assert dataset.ontology is not None
    assert len(dataset.ontology) > 0
    assert len(dataset.user_intents) > 0
    assert len(dataset.system_actions) > 0
    assert callable(dataset.database)
    assert isinstance(dataset.database({}), dict)
    assert isinstance(dataset[0].database, OrderedDict)
    assert isinstance(dataset[0].raw_belief, OrderedDict)
