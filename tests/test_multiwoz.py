import random
from data.utils import format_belief, BeliefParser
from collections import OrderedDict
import data
from data.convlab_dataset import match_results, AugmentTransformation
from data.convlab_dataset.matcher import normalize_for_test
from data.convlab_dataset.nlp import normalize, denormalize
import pytest


@pytest.fixture(scope='module')
def augmenter():
    return AugmentTransformation()


def _safe_load(name):
    try:
        dataset = data.load_dataset(name, output_assignment=True)
        transform = dataset.transform
        random.seed(1 * 42)
        num_samples = 50
        old_items = list(dataset.items)
        dataset.items = random.sample(dataset.items, num_samples)
        raw_items = list(dataset.items)
        dataset = dataset.finish()
        setattr(dataset, '__transform', transform)
        setattr(dataset, '__items', raw_items)
        setattr(dataset, '__olditems', old_items)
        return dataset

    except Exception as e:
        return e


dataset_names = ["multiwoz-train", "multiwoz-val", "multiwoz-test"]
datasets = {name: _safe_load(name) for name in dataset_names}


def assert_dict_same_keys(d1, d2):
    intersection = set(d1.keys()).intersection(d2.keys())
    union = set(d1.keys()).union(d2.keys())
    assert len(intersection) == len(union), f'different keys {union - intersection}'


def assert_belief_same_keys(b1, b2):
    assert_dict_same_keys(b1, b2)
    for k in b1.keys():
        assert_dict_same_keys(b1[k], b2[k])


@pytest.fixture(scope="module")
def dataset(request):
    return datasets[dataset_names[0]]


@pytest.fixture(scope="module")
def parse_belief(dataset):
    return BeliefParser(dataset.domains)


@pytest.mark.parametrize('dataset_name', dataset_names)
def test_taskmaster_loads(dataset_name):
    val = datasets[dataset_name]
    if isinstance(val, Exception):
        raise val
    elif not isinstance(val, data.DialogDataset):
        raise ValueError(f'Dataset has invalid type {type(val)}, it should be DialogDataset')


def pytest_generate_tests(metafunc):
    if "sample" in metafunc.fixturenames:
        samples = []
        raw_samples = []
        for dataset in datasets.values():
            if not isinstance(dataset, data.DialogDataset):
                continue
            samples.extend(dataset)
            raw_samples.extend(dataset.__items)

        metafunc.parametrize(("raw_sample", 'sample'), list(zip(raw_samples, samples)))


def test_parse_format_belief(dataset, parse_belief, raw_sample, sample):
    assert isinstance(sample.raw_belief, OrderedDict)
    parsed = parse_belief(sample.belief)
    assert isinstance(parsed, OrderedDict)
    new_belief = format_belief(parsed)
    assert sample.belief == new_belief


def test_augment_format_belief(dataset, augmenter, raw_sample, sample):
    augmented = augmenter(sample)
    assert_belief_same_keys(augmented.raw_belief, sample.raw_belief)


def test_lexicalize(dataset, parse_belief, raw_sample, sample):
    raw_belief, raw_response, delex_response = sample.raw_belief, sample.raw_response, sample.response
    database_results = dataset.database(raw_belief, return_results=True)
    # TODO: assert matched in database_results
    matched_results = match_results(sample.assignment, database_results)
    new_response = dataset.lexicalizer(delex_response, matched_results, raw_belief, sample.context)

    print('CTX:', sample.context)
    print('Belief:', sample.belief)
    print('DB:', database_results)
    print('assignment:', sample.assignment)
    print('DT response:', sample.response)
    print('raw r:', raw_response)
    print('new r:', new_response)
    # target = denormalize(normalize(raw_response))
    assert normalize_for_test(denormalize(raw_response).lower()) == \
        normalize_for_test(denormalize(new_response).lower())


def test_add_apostrophs(dataset, parse_belief):
    item = dataset.__olditems[2077]
    transformed = dataset.__transform(item)
    assert parse_belief(transformed.belief)['attraction']['name'] == "great saint mary's church"


def test_correct_types(dataset):
    assert isinstance(dataset, data.DialogDataset)
    assert dataset.database is not None
    assert callable(dataset.database)
    assert isinstance(dataset.database({}), dict)
    assert isinstance(dataset[0].database, OrderedDict)
    assert dataset.ontology is not None
    assert len(dataset.ontology) > 0
    assert len(dataset.user_intents) > 0
    assert len(dataset.system_actions) > 0
