#!/bin/env python
if __name__ == '__main__':
    import sys
    import os
    import logging
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

    from data import load_dataset
    from data.convlab_dataset.dataset import BeliefStateTransformation, normalize
    from tqdm import tqdm
    from utils import setup_logging

    domains = ['restaurant', 'hotel', 'attraction', 'train', 'taxi', 'hospital', 'police']
    belief_transformation = BeliefStateTransformation()
    setup_logging()
    logger = logging.getLogger()

    def parse_goal_domain(gt_belief, goal, dialog_goal, domain):
        """Parses user goal into dictionary format."""
        goal[domain] = {}
        goal[domain] = {'informable': [], 'requestable': [], 'booking': []}
        if 'info' in dialog_goal[domain]:
            # if d['goal'][domain].has_key('info'):
            if domain == 'train':
                # we consider dialogues only where train had to be booked!
                if 'book' in dialog_goal[domain]:
                    # if d['goal'][domain].has_key('book'):
                    goal[domain]['requestable'].append('reference')
                if 'reqt' in dialog_goal[domain]:
                    # if d['goal'][domain].has_key('reqt'):
                    if 'trainID' in dialog_goal[domain]['reqt']:
                        goal[domain]['requestable'].append('id')
            else:
                if 'reqt' in dialog_goal[domain]:
                    # if d['goal'][domain].has_key('reqt'):
                    for s in dialog_goal[domain]['reqt']:  # addtional requests:
                        if s in ['phone', 'address', 'postcode', 'reference', 'id']:
                            # ones that can be easily delexicalized
                            goal[domain]['requestable'].append(s)
                if 'book' in dialog_goal[domain]:
                    # if d['goal'][domain].has_key('book'):
                    goal[domain]['requestable'].append("reference")

            goal[domain]["informable"] = dialog_goal[domain]['info']
            if 'book' in dialog_goal[domain]:
                # if d['goal'][domain].has_key('book'):
                goal[domain]["booking"] = dialog_goal[domain]['book']
        belief = {domain: {'semi': goal[domain]['informable']}}
        belief = belief_transformation(belief, [], domain).get(domain, dict())
        for key, val in gt_belief.get(domain, dict()).items():
            if key in belief:
                belief[key] = val
        goal[domain]['informable'] = belief
        return goal

    def parse_goal(gt_belief, dialog_goal):
        domains = ['restaurant', 'hotel', 'attraction', 'train', 'taxi', 'hospital', 'police']
        goal = {}
        for domain in domains:
            if dialog_goal[domain]:
                goal = parse_goal_domain(gt_belief, goal, dialog_goal, domain)
        return goal

    def is_booked(raw_belief, domain):
        return domain in raw_belief and 'book' in raw_belief[domain] and \
            'booked' in raw_belief[domain]['book'] and \
            any('reference' in x for x in raw_belief[domain]['book']['booked'])

    d1 = load_dataset('multiwoz-train', use_goal=True, _multiwoz_raw_belief=True)
    d2 = load_dataset('multiwoz-2.1-train', use_goal=True)
    logger.setLevel(logging.DEBUG)
    difference = 0
    for i, (i1, i2, _) in enumerate(tqdm(zip(d1, d2, range(5000)), total=5000)):
        b1, b2 = i1.raw_belief, i2.raw_belief
        # if 'booked' in b1: b1.pop('booked')
        # if 'booked' in b2: b2.pop('booked')
        i1.context = list(map(normalize, i1.context))
        g1 = parse_goal(b1, i1.goal)
        g1 = (g1.get('informable', None), g1.get('requestable', None))
        g2 = i2.goal
        g2 = (g2.get('informable', None), g2.get('requestable', None))
        different = False
        if i1.active_domain != i2.active_domain:
            logger.debug(f'{i}: {i1.active_domain} != {i2.active_domain}')
            different = True
        elif repr(b1) != repr(b2):
            logger.debug(f'{i}: {repr(b1)} != {repr(b2)}')
            different = True
        elif repr(i1.database) != repr(i2.database):
            logger.debug(f'{i}: database')
            different = True
            logger.debug(f'{i}: {repr(i1.database)} != {repr(i2.database)}')
        else:
            book1 = list()
            book2 = list()
            for d, bs in b2.items():
                bk2 = bs.get('booked', 'false')
                if bk2 == 'true':
                    book2.append(d)
                bk1 = is_booked(i1._multiwoz_raw_belief, d)
                if bk1:
                    book1.append(d)
            if repr(book1) != repr(book2):
                logger.debug(f'{i}: book')
                logger.debug(f'{i}: {repr(book1)} != {repr(book2)}')
                different = True

        if repr(g1) != repr(g2):
            logger.debug(f'{i}: goal')
            logger.debug(f'{i}: {repr(g1)} != {repr(g2)}')
            different = True
        if repr(g1) != repr(g2):
            logger.debug(f'{i}: goal')
            logger.debug(f'{i}: {repr(g1)} != {repr(g2)}')
            different = True
        if repr(i1.context) != repr(i2.context):
            logger.debug(f'{i}: context')
            logger.debug(f'{i1.context} != {i2.context}')
            different = True
        if repr(i1.response) != repr(i2.response):
            logger.debug(f'{i}: response')
            logger.debug(f'{i1.response} != {i2.response}')
            different = True
        if repr(i1.raw_response) != repr(i2.raw_response):
            logger.debug(f'{i}: raw response')
            different = True
        if different:
            difference += 1
        # if repr(b1) != repr(b2):
        #    print(f'"{repr(b1)}" != "{repr(b2)}"')
    logger.info(f'Difference is {difference * 100 / 5000:.2f}%')
