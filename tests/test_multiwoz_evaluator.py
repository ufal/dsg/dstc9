from data.convlab_dataset.evaluate import MultiWozEvaluator
from data.convlab_dataset import load_convlab_dataset
from data.utils import wrap_dataset_with_cache


def test_multiwoz_evaluator():
    dataset = 'multiwoz-train'
    dataset = load_convlab_dataset(dataset, goal=True, use_blacklist=False, _multiwoz_raw_belief=True)

    # There is a bad dialog PMUL1812
    # dataset.items = dataset.items[:838] + dataset.items[838 + 6:]
    dataset.items = dataset.items[239 + 127:]

    dataset = wrap_dataset_with_cache(dataset)
    responses = (item.response for item in dataset)
    multiwoz_evaluator = MultiWozEvaluator(dataset)
    beliefs = (multiwoz_evaluator.belief_parser(item.belief) for item in dataset)
    success, match, stats = multiwoz_evaluator.evaluate(beliefs, responses)
    print(f'match: {match}, success: {success}')
    for domain, (match, success) in stats.items():
        print(f'   - domain: {domain}, match: {match:.4f}, success: {success:.4f}')

    assert match == 1.0
    assert success == 1.0
