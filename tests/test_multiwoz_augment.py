from data.convlab_dataset.augment import Augmenter
from data.utils import DialogDatasetItem, format_belief
from collections import OrderedDict
from pytest import fixture


@fixture(scope='module')
def augmenter():
    return Augmenter()


def test_augment_dialog_item(augmenter):
    augmenter.seed(42)
    item = DialogDatasetItem(['test'], response='', raw_response='')
    item = augmenter(item)
    assert isinstance(item, DialogDatasetItem)


def test_augment_phone(augmenter):
    augmenter.seed(42)
    sentence = 'the phone is 728 255 255'
    augmented = augmenter.augment_sentences([sentence])[0]
    assert augmented == 'the phone is 602 768 402'


def test_augment_reference(augmenter):
    augmenter.seed(42)
    sentence = 'this is a dummy sentence RD235G23'
    augmented = augmenter.augment_sentences([sentence])[0]
    assert augmented == 'this is a dummy sentence 098QOW3F'


def test_augment_stars(augmenter):
    augmenter.seed(42)
    sentence = 'this is a 3 stars hotel'
    augmented = augmenter.augment_sentences([sentence])[0]
    assert augmented == 'this is a 5 stars hotel'


def test_augment_id(augmenter):
    augmenter.seed(42)
    sentence = 'this is a dummy sentence TR1234'
    augmented = augmenter.augment_sentences([sentence])[0]
    assert augmented == 'this is a dummy sentence TR1824'


def test_time(augmenter):
    augmenter.seed(42)
    sentence = 'this train leaves at 16:45 and arrives at 18 pm'
    augmented = augmenter.augment_sentences([sentence])[0]
    assert augmented == 'this train leaves at 13:45 and arrives at 15 pm'


def test_time_overflow(augmenter):
    augmenter.seed(42)
    sentence = 'this train leaves at 23:59 and arrives at 23 pm'
    augmented = augmenter.augment_sentences([sentence])[0]
    assert augmented == 'this train leaves at 20:59 and arrives at 20 pm'


def test_price(augmenter):
    augmenter.seed(42)
    sentence = 'this train costs 3 pounds'
    augmented = augmenter.augment_sentences([sentence])[0]
    assert augmented == 'this train costs 2.40 pounds'


def test_number(augmenter):
    augmenter.seed(42)
    sentence = 'this train costs 3 pounds'
    augmented = augmenter.augment_sentences([sentence])[0]
    assert augmented == 'this train costs 2.40 pounds'


def test_number_str(augmenter):
    augmenter.seed(42)
    sentence = 'this is number four and there are no more 4'
    augmented = augmenter.augment_sentences([sentence])[0]
    assert augmented == 'this is number nine and there are no more 9'


def test_database_results_number(augmenter):
    augmenter.seed(42)
    sentence = 'there are six trains'
    item = DialogDatasetItem(context=[sentence], database=dict(test=6), response='', raw_response='')
    item = augmenter(item)
    assert item.context[0] == 'there are nine trains'
    assert item.database['test'] == 9


def test_database_results_multiple(augmenter):
    augmenter.seed(42)
    sentence = 'there are six trains'
    item = DialogDatasetItem(context=[sentence], database=OrderedDict(test=6, test2=3), response='', raw_response='')
    item = augmenter(item)
    assert item.context[0] == 'there are nine trains'
    assert item.database['test'] == 9
    assert item.database['test2'] == 3


def test_belief(augmenter):
    augmenter.seed(42)
    belief = OrderedDict(restaurant=OrderedDict(food='italian'))
    item = DialogDatasetItem(context=[], raw_belief=belief, belief=format_belief(belief), response='', raw_response='')
    item = augmenter(item)
    print(item)
    assert item.raw_belief['restaurant']['food'] == 'portuguese'


def test_disabled():
    augmenter = Augmenter('disabled')
    augmenter.seed(42)
    belief = OrderedDict(restaurant=OrderedDict(food='italian'))
    item = DialogDatasetItem(context=[], raw_belief=belief, belief=format_belief(belief), response='', raw_response='')
    item = augmenter(item)
    print(item)
    assert item.raw_belief['restaurant']['food'] == 'italian'


def test_price_range(augmenter):
    augmenter.seed(42 + 3)
    sentence = 'this train is expensive'
    augmented = augmenter.augment_sentences([sentence])[0]
    assert augmented == 'this train is moderate'


def test_price_range_2(augmenter):
    augmenter.seed(42 + 3)
    sentence = 'this train is expensively priced'
    augmented = augmenter.augment_sentences([sentence])[0]
    assert augmented == 'this train is moderately priced'


def test_food(augmenter):
    augmenter.seed(42 + 3)
    sentence = 'this restaurant server italian food only'
    augmented = augmenter.augment_sentences([sentence])[0]
    assert augmented == 'this restaurant server swedish food only'


def test_postcode(augmenter):
    augmenter.seed(42)
    sentence = 'this restaurant server CB23EC'
    augmented = augmenter.augment_sentences([sentence])[0]
    assert augmented == 'this restaurant server CB14AH'
