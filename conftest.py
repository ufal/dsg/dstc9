def pytest_configure(config):
    plugin = config.pluginmanager.getplugin('mypy')
    if plugin is not None:
        plugin.mypy_argv.append('--check-untyped-defs')
