#!/bin/env python
import os
import sys
import argparse
import logging
import wandb
import torch
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from utils import setup_logging  # noqa:E402
import agent  # noqa: E402
import data.evaluation  # noqa: E402


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', default='20c9xa0e')
    parser.add_argument('--dataset', default='multiwoz-2.1-test')
    parser.add_argument('--num_dialogs', type=int, default=100)
    parser.add_argument('--top_p', type=float, default=0.2)
    parser.add_argument('--num_beams', type=int, default=1)
    parser.add_argument('--resume', default=None)
    parser.add_argument('--no-wandb', action='store_true')
    args = parser.parse_args()
    setup_logging()
    logger = logging.getLogger()

    if not args.no_wandb:
        if not args.resume:
            # It is an artifact
            # Start a new evaluate run
            wandb.init(project='dstc9', entity='dstc', job_type='evaluation', config=args)
            args = argparse.Namespace(**wandb.config)
        else:
            # Resume run and fill metrics
            os.environ.pop('WANDB_NAME', None)
            wandb.init(project='dstc9', entity='dstc', resume=args.resume)

    analyzer = data.evaluation.multiwoz.ConvLabAnalyzer()
    logger.info('loading model')
    agent = agent.AutoDialogAgent.from_pretrained(args.model, args.dataset)
    if torch.cuda.is_available():
        agent = agent.to('cuda')
    if args.top_p is not None:
        agent.predictor.model.config.top_p = args.top_p
    if args.num_beams is not None:
        agent.predictor.model.config.num_beams = args.num_beams

    # Analyze
    result = analyzer(agent, args.num_dialogs)
    if wandb.run:
        wandb.run.summary.update(result)
