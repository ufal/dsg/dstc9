import os
import sys
import argparse
import logging
import torch
import wandb
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from train_multiwoz import MultiWozTrainer  # noqa:E402
from utils import pull_model, setup_logging  # noqa:E402

parser = argparse.ArgumentParser()
parser.add_argument('run')
args = parser.parse_args()
run = args.run
root = pull_model(run)
args = torch.load(os.path.join(root, 'training_args.bin'))

setup_logging()
logger = logging.getLogger()
args.epochs = 0
args.local_rank = -1
args.model = run
if not hasattr(args, 'clean_samples'):
    args.clean_samples = False
os.environ.pop('WANDB_NAME', None)
wandb.init(project='dstc9', resume=run)
trainer = MultiWozTrainer(args, logger)
trainer._update_config_and_args = lambda *args, **kwargs: None
trainer._save = lambda: None
trainer._publish_artifact = lambda: None
trainer._initialize_logging = lambda: None
logger.info('finishing failed run')
trainer._synchronize_wandb_runid()

trainer.train()
