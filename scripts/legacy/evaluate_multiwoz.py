#!/bin/env python
import os
import sys
import argparse
import logging
import wandb
import torch
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from utils import setup_logging, pull_model  # noqa:E402
import agent  # noqa: E402
from data.utils import BeliefParser, wrap_dataset_with_cache  # noqa: E402
from data import load_dataset  # noqa: E402
from data.convlab_dataset.evaluate import MultiWozEvaluator, compute_bleu_remove_reference  # noqa: E402
from evaluate import generate_predictions, compute_delexicalized_bleu  # noqa:E402


def parse_predictions(dataset, filename):
    gts, bfs, rs = [], [], []
    delexrs, delexgts = [], []
    parser = BeliefParser(dataset.domains)
    with open(filename, 'r') as f:
        for line in f:
            line = line.rstrip()
            if line.startswith('GT:'):
                gts.append(line[len('GT:'):])
            elif line.startswith('GTD:'):
                delexgts.append(line[len('GTD:'):])
            elif line.startswith('BF:'):
                bf = line[len('BF:'):]
                bf = parser(bf)
                assert bf is not None
                bfs.append(bf)
            elif line.startswith('RD:'):
                delexrs.append(line[len('RD:'):])
            elif line.startswith('R:'):
                r = line[len('R:'):]
                rs.append(r)
    # assert len(gts) == len(bfs) == len(rs) == len(delexrs) == len(delexgts)
    return rs, bfs, gts, delexrs, delexgts


parser = argparse.ArgumentParser()
parser.add_argument('--model', default=None)
parser.add_argument('--file', default=None)
parser.add_argument('--resume', default=None)
parser.add_argument('--dataset', default='multiwoz-test')
parser.add_argument('--no-wandb', action='store_true')
args = parser.parse_args()
if args.resume is not None and args.model is None:
    args.model = args.resume
assert args.model is not None or args.file is not None

setup_logging()
logger = logging.getLogger()
if not args.no_wandb:
    if not args.resume:
        # It is an artifact
        # Start a new evaluate run
        wandb.init(project='dstc9', entity='dstc', job_type='evaluation')
    else:
        # Resume run and fill metrics
        os.environ.pop('WANDB_NAME', None)
        wandb.init(project='dstc9', entity='dstc', resume=args.resume)

dataset = load_dataset(args.dataset, goal=True, _multiwoz_raw_belief=True)
dataset = wrap_dataset_with_cache(dataset)

if args.file is not None:
    if args.file is None or not os.path.exists(args.file):
        model = pull_model(args.model)

if args.file is not None:
    path = args.file
    if not os.path.exists(path):
        path = os.path.join(model, args.file)
    responses, beliefs, gold_responses, delex_responses, delex_gold_responses = parse_predictions(dataset, path)
else:
    logger.info('generating responses')
    agent = agent.AutoDialogAgent.from_pretrained(args.model, dataset=dataset)
    if torch.cuda.is_available():
        agent = agent.to('cuda')
    responses, beliefs, gold_responses, delex_responses, delex_gold_responses = \
        generate_predictions(agent, dataset, os.path.join(wandb.run.dir if wandb.run else '.', 'test-predictions.txt'))
logger.info('evaluation started')
evaluator = MultiWozEvaluator(dataset, is_multiwoz_eval=True, logger=logger)
success, matches, domain_results = evaluator.evaluate(beliefs, delex_responses, progressbar=True)
logger.info('evaluation finished')
logger.info(f'match: {matches:.4f}, success: {success:.4f}')
logger.info('computing bleu')
bleu = compute_bleu_remove_reference(responses, gold_responses)
logger.info(f'test bleu: {bleu:.4f}')
delex_bleu = compute_delexicalized_bleu(delex_responses, delex_gold_responses)
logger.info(f'test delex bleu: {delex_bleu:.4f}')
if wandb.run:
    wandb.run.summary.update(dict(
        test_inform=matches,
        test_success=success,
        test_bleu=bleu,
        test_delex_bleu=delex_bleu,
    ))
