#!/bin/env python
import wandb
import argparse
import logging
import tempfile
from collections import Counter
import os
import sys
from tqdm import tqdm
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from data.convlab_dataset.evaluate import MultiWozEvaluator  # noqa: E402
from data.convlab_dataset import load_convlab_dataset  # noqa: E402
from data.utils import format_belief, wrap_dataset_with_cache  # noqa: E402
from utils import setup_logging  # noqa: E402


def enumerate_multiwoz_invalid_indices(dataset, logger):
    dataset = load_convlab_dataset(dataset, goal=True, use_blacklist=False, _multiwoz_raw_belief=True)
    dataset = wrap_dataset_with_cache(dataset)
    evaluator = MultiWozEvaluator(dataset, logger=logger, is_multiwoz_eval=True)
    responses = (item.response for item in dataset)
    beliefs = (evaluator.belief_parser(item.belief) for item in dataset)
    dialogues = evaluator.pack_dialogues(dataset, beliefs, responses)
    successes, matches = 0, 0
    stats = tuple(Counter() for _ in range(3))
    domain_total, domain_match, domain_success = stats
    total = 0

    offset = 0
    with tqdm(total=len(dataset), desc='identifying bad dialogues') as progress:
        for idx, (items, dialog_goal, beliefs, responses) in enumerate(dialogues):
            goal, real_requestables = evaluator._get_goal_and_requestables(items[-1].raw_belief, dialog_goal)
            raw_beliefs = [x._multiwoz_raw_belief for x in items]
            provided_requestables, venue_offered = \
                evaluator._get_requestables_and_venues(beliefs, responses, raw_beliefs)
            success, match = evaluator._evaluate_generated_dialogue(
                real_requestables, provided_requestables, venue_offered, goal, stats)
            if match != 1 or success != 1:
                for i in range(offset, offset + len(items)):
                    yield f'{i}'
            elif len(set(map(format_belief, beliefs))) == 1 and len(items) > 1:
                match, success = 0, 0
                for i in range(offset, offset + len(items)):
                    yield f'{i}'

            successes += success
            matches += match
            total += 1
            offset += len(items)
            progress.update(len(items))

        domain_results = dict()
        for key in domain_total.keys():
            domain_results[key] = domain_match[key] / float(domain_total[key]), \
                domain_success[key] / float(domain_total[key])

        match, success = matches / float(total), successes / float(total)
        logger.info(f'match: {match:.4f}, success: {success:.4f}')
        for domain, (match, success) in domain_results.items():
            logger.info(f'   - domain: {domain}, match: {match:.4f}, success: {success:.4f}')


if __name__ == '__main__':
    setup_logging()
    logger = logging.getLogger()
    parser = argparse.ArgumentParser()
    parser.add_argument('--no-upload', action='store_true')
    parser.add_argument('--dataset', default='multiwoz')
    args = parser.parse_args()
    if not args.no_upload:
        wandb.init(entity='dstc', project='dstc9', job_type='preprocessing')
        artifact = wandb.Artifact(f'{args.dataset}-blacklist', 'dataset')
    sets = ['test', 'val', 'train']
    for dataset_type in sets:
        if wandb.run:
            f = artifact.new_file(f'{dataset_type}-blacklist.txt')
        else:
            f = tempfile.TemporaryFile('w+')
        with f:
            for bad_session_id in enumerate_multiwoz_invalid_indices(f'{args.dataset}-{dataset_type}', logger):
                print(bad_session_id, file=f)
                f.flush()
    if wandb.run:
        wandb.run.log_artifact(artifact)
