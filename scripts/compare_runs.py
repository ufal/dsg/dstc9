import argparse
from enum import Enum
import wandb
import numpy as np


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    feat_choices = ['unlike', 'clean', 'aux', 'pre-mw']
    parser.add_argument('--feature', choices=feat_choices, required=True)
    parser.add_argument('--feature2', choices=feat_choices, required=False)
    parser.add_argument('--iterations', type=str, required=False, help='comma separated list of iterations considered')
    args = parser.parse_args()

    api = wandb.Api()
    all_runs = api.runs('dstc/dstc9')
    tags = ['iter1', 'iter2', 'iter3', 'iter4']
    if args.iterations is not None:
        tags = args.iterations.split(',')
    metrics = ['test_success', 'test_bleu', 'success_rate', 'match', 'f1', 'complete_rate']
    metrics_diff_dict = {m: [] for m in metrics}

    for t in tags:
        runs_with_feature = []
        runs_wo_feature = []
        for r in all_runs:
            if not t in r.tags or \
                'mw' not in r.name or \
                r.state != 'finished':
            #    'rainbow' in r.name:
                continue
            if r.config['response_loss'] == 'unlikelihood' and r.config['clean_samples'] == True:
            #if args.feature in r.name:
                runs_with_feature.append(r)
            #elif args.feature2 is not None:
             #   if args.feature2 in r.name:
              #      runs_wo_feature.append(r)
            elif r.config['clean_samples'] == True:
                runs_wo_feature.append(r)

        for rf in runs_with_feature:
            for rnf in runs_wo_feature:
                for metric in metrics:
                    if metric not in rf.summary:
                        print(f'Run "{rf.name} ({rf.id})" did not record the metric: {metric}')
                    elif metric not in rnf.summary:
                        print(f'Run "{rnf.name} ({rnf.id})" did not record the metric: {metric}')
                    else:
                        metrics_diff_dict[metric].append(rf.summary[metric] - rnf.summary[metric])

    for metric, diff in metrics_diff_dict.items():
        print('{0:>15}:\tmean: {1:2.4f}\tstd: {2:2.4f}'.format(metric, np.mean(diff), np.std(diff)))
