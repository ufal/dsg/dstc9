#!/bin/bash

# Clone transformers (optional)
rm -rf transformers
git clone https://github.com/huggingface/transformers.git --branch v3.3.1 --single-branch transformers
cd transformers
git checkout -b local
cd ..

artifact=$(python -c "import wandb; print(wandb.Api().artifact('dstc/dstc9/multiwoz-finetune:latest').download())")
artifact=$(realpath $artifact)
output_dir=$(realpath output)
mkdir -p output_dir

export WANDB_ENTITY=dstc
export WANDB_PROJECT=dstc9
cd transformers/examples/seq2seq
python finetune.py \
    --learning_rate=3e-5 \
    --fp16 \
    --gpus 1 \
    --do_train \
    --do_predict \
    --n_val 1000 \
    --num_train_epochs 6 \
    --val_check_interval 0.2 \
    --data_dir "$artifact" \
    --output_dir "$output_dir" \
    --model_name_or_path "sshleifer/distilbart-xsum-12-3" \
    --logger_name wandb \
    "$@"
cd ../../../

rm -rf transformers
