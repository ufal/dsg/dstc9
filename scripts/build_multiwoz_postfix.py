#!/bin/env python
import wandb
import argparse
import logging
import os
import sys
from tqdm import tqdm
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from data.convlab_dataset import load_convlab_dataset  # noqa: E402
from data.utils import wrap_dataset_with_cache  # noqa: E402
from utils import setup_logging  # noqa: E402


if __name__ == '__main__':
    setup_logging()
    logger = logging.getLogger()
    parser = argparse.ArgumentParser()
    args = parser.parse_args()
    wandb.init(entity='dstc', project='dstc9', job_type='preprocessing')
    artifact = wandb.Artifact('multiwoz-finetune', 'dataset')
    sets = ['test', 'val', 'train']
    for dataset_type in sets:
        dataset = load_convlab_dataset(f'multiwoz-{dataset_type}', output_assignment=True)
        dataset = wrap_dataset_with_cache(dataset)
        with artifact.new_file(f'{dataset_type}.source') as sf, artifact.new_file(f'{dataset_type}.target') as tf:
            for sample in tqdm(dataset, desc='generating finetune dataset'):
                raw_belief, raw_response, delex_response = sample.raw_belief, sample.raw_response, sample.response
                database_results = dataset.database(raw_belief, return_results=True)
                matched_results = dataset.lexicalizer.match_results(sample.assignment, database_results)
                new_response = dataset.lexicalizer(delex_response, matched_results, raw_belief, sample.context)

                print(new_response, file=sf)
                print(raw_response, file=tf)
            sf.flush()
            tf.flush()
    wandb.run.log_artifact(artifact)
