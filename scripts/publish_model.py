import os
import sys
import argparse
import logging
import wandb
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from train import setup_logging  # noqa: E402
from utils import pull_model, publish_model  # noqa: E402

parser = argparse.ArgumentParser()
parser.add_argument('run')
parser.add_argument('--name', default=None, help='artifact name')
args = parser.parse_args()
run = args.run
root = pull_model(run)
setup_logging()
logger = logging.getLogger()
logger.info('publishing artifact')
wandb.init(project='dstc9', entity='dstc', resume=run)
publish_model(root, args.name)
logger.info('model published')
