import re
import logging
import torch
import operator
import argparse
from functools import reduce, partial
from data.utils import InsertLabelsTransformation, format_belief
from agent import AutoDialogAgent
from utils import pull_model, setup_logging
import nltk
from tqdm import tqdm


def generate_predictions(agent, dataset, output_file='predictions.txt'):
    add_labels = InsertLabelsTransformation('U:', 'S:', 'D:', 'BF:')
    gold_responses = []
    responses = []
    delex_responses = []
    delex_gold_responses = []
    beliefs = []
    with open(output_file, 'w+') as fout:
        d = 0
        for i, sample in enumerate(tqdm(dataset, desc='generating predictions')):
            if len(sample.context) == 1:
                d += 1
                print(f'======== dialogue {d} ========', file=fout)
            print(f'U:{sample.context[-1]}', file=fout)
            print(f'GT:{sample.raw_response}', file=fout)
            print(f'GTD:{sample.response}', file=fout)
            gold_responses.append(sample.raw_response)
            delex_gold_responses.append(sample.response)
            response = None
            results = agent([sample.context], lexicalise=dataset.lexicalizer is not None)[0]
            if dataset.lexicalizer is not None:
                response = results[3]
            raw_belief, database, raw_response = results[:3]
            belief = format_belief(raw_belief)
            sample = add_labels((sample.context, belief, database, response or raw_response, 1))
            print(sample.belief, file=fout)
            print(sample.database, file=fout)
            if response:
                print(f'R:{sample.response}', file=fout)
            else:
                print('R:', file=fout)
            print(f'RD:{raw_response}', file=fout)
            beliefs.append(raw_belief)
            responses.append(response)
            delex_responses.append(raw_response)
    return responses, beliefs, gold_responses, delex_responses, delex_gold_responses


def compute_bleu(responses, gold_responses):
    responses = map(lambda x: x.lower(), responses)
    responses = list(map(nltk.tokenize.word_tokenize, responses))
    gold_responses = map(lambda x: x.lower(), gold_responses)
    gold_responses = map(nltk.tokenize.word_tokenize, gold_responses)
    gold_responses = list(map(lambda x: [x], gold_responses))
    bleu = nltk.translate.bleu_score.corpus_bleu(gold_responses, responses)
    return bleu


def compute_delexicalized_bleu(responses, gold_responses):
    token_regex = re.compile(r'\[([\w\s\d]+)\]')
    token_sub = partial(token_regex.sub, lambda x: x.group(1).upper().replace(' ', ''))
    responses = map(lambda x: x.lower(), responses)
    responses = map(token_sub, responses)
    responses = list(map(nltk.tokenize.word_tokenize, responses))
    gold_responses = map(lambda x: x.lower(), gold_responses)
    gold_responses = map(token_sub, gold_responses)
    gold_responses = map(nltk.tokenize.word_tokenize, gold_responses)
    gold_responses = list(map(lambda x: [x], gold_responses))
    bleu = nltk.translate.bleu_score.corpus_bleu(gold_responses, responses)
    return bleu


def compute_sentence_bleu(response, gold_responses):
    responses = nltk.tokenize.sent_tokenize(response.lower())
    responses = list(map(nltk.tokenize.word_tokenize, responses))
    gold_responses = reduce(operator.add, (nltk.tokenize.sent_tokenize(x.lower()) for x in gold_responses))
    gold_responses = list(map(nltk.tokenize.word_tokenize, gold_responses))
    bleu = nltk.translate.bleu_score.corpus_bleu(gold_responses, responses)
    return bleu


if __name__ == '__main__':
    from data.evaluation import ConvLabAnalyzer
    parser = argparse.ArgumentParser()
    parser.add_argument('model')
    parser.add_argument('--dataset', default='multiwoz-2.1-test')
    parser.add_argument('--num-dialogs', type=int, default=1000)
    parser.add_argument('--top-p', type=float, default=None)
    parser.add_argument('--num-beams', type=int, default=None)
    args = parser.parse_args()
    setup_logging()
    logger = logging.getLogger()

    # Update punkt
    nltk.download('punkt')

    analyzer = ConvLabAnalyzer()
    logger.info('loading model')
    model_name = pull_model(args.model)
    agent = AutoDialogAgent.from_pretrained(model_name, args.dataset)
    if torch.cuda.is_available():
        agent = agent.to('cuda')
    if args.top_p is not None:
        agent.predictor.model.config.top_p = args.top_p
    if args.num_beams is not None:
        agent.predictor.model.config.num_beams = args.num_beams

    # Analyze
    analyzer(agent, args.num_dialogs)
