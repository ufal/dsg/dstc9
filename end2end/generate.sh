#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
rm -rf $DIR/submission*
i=1
while read modelname; do
    if [ -z "$modelname" ]
        then continue
    fi
    mkdir "$DIR/submission$i"
    cat <<EOT >> "$DIR/submission$i/automatic.py"
#!/bin/env python
import os, sys  # noqa:E401
from convlab2.util.analysis_tool.analyzer import Analyzer
from convlab2.nlu.jointBERT.multiwoz import BERTNLU
from convlab2.policy.rule.multiwoz import RulePolicy
from convlab2.nlg.template.multiwoz import TemplateNLG
from convlab2.dialog_agent import PipelineAgent
import random
import numpy as np
import torch
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
os.environ['WANDB_ANONYMOUS'] = 'must'

from data.evaluate.multiwoz import ConvLabWrapper  # noqa:E402
from agent import AutoDialogAgent  # noqa:E402


def set_seed(r_seed):
    random.seed(r_seed)
    np.random.seed(r_seed)
    torch.manual_seed(r_seed)


def automatic_analyze(sys_agent):
    sys_agent = ConvLabWrapper(sys_agent)

    user_nlu = BERTNLU(mode='sys', config_file='multiwoz_sys_context.json',
                       model_file='https://convlab.blob.core.windows.net/convlab-2/bert_multiwoz_sys_context.zip')
    user_dst = None
    user_policy = RulePolicy(character='usr')
    user_nlg = TemplateNLG(is_user=True)
    user_agent = PipelineAgent(user_nlu, user_dst, user_policy, user_nlg, name='user')
    analyzer = Analyzer(user_agent, dataset='multiwoz')

    set_seed(20200202)
    analyzer.comprehensive_analyze(sys_agent=sys_agent, model_name='end2end', total_dialog=100)


if __name__ == '__main__':
    agent = AutoDialogAgent.from_pretrained('${modelname}')
    automatic_analyze(agent)
EOT
    chmod +x "$DIR/submission$i/automatic.py"

    cat <<EOT >> "$DIR/submission$i/human.py"
#!/bin/env python
import os, sys  # noqa:E401
import copy
from flask import Flask, request, jsonify
from queue import Queue
from threading import Thread
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
os.environ['WANDB_ANONYMOUS'] = 'must'

from evaluate import ConvLabWrapper  # noqa:E402
from agent import AutoDialogAgent  # noqa:E402


def start_http_agent(agent):
    agent = ConvLabWrapper(agent)

    # Agent
    rgi_queue = Queue(maxsize=0)
    rgo_queue = Queue(maxsize=0)
    app = Flask(__name__)

    # Test out if our agent works
    # Otherwize fail
    agent.response('I am looking for a hotel')
    agent.init_session()

    @app.route('/', methods=['GET', 'POST'])
    def process():
        try:
            in_request = request.json
            print(in_request)
        except Exception:
            return "invalid input: {}".format(in_request)
        rgi_queue.put(in_request)
        rgi_queue.join()
        output = rgo_queue.get()
        print(output['response'])
        rgo_queue.task_done()
        # return jsonify({'response': response})
        return jsonify(output)

    def generate_response(in_queue, out_queue):
        while True:
            # pop input
            last_action = 'null'
            in_request = in_queue.get()
            obs = in_request['input']
            if 'agent_state' not in in_request or not in_request['agent_state']:
                agent.init_session()
            else:
                encoded_state, dst_state, last_action = in_request['agent_state']
                agent.history = copy.deepcopy(dst_state)
            try:
                action = agent.response(obs)
                print(f'obs:{obs}; action:{action}')
                dst_state = copy.deepcopy(agent.history)
                encoded_state = None
            except Exception as e:
                print('agent error', e)

            try:
                if action == '':
                    response = 'Sorry I do not understand, can you paraphrase?'
                else:
                    response = action
            except Exception as e:
                print('Response generation error', e)
                response = 'What did you say?'

            last_action = action
            out_queue.put({'response': response, 'agent_state': (encoded_state, dst_state, last_action)})
            in_queue.task_done()
            out_queue.join()

    worker = Thread(target=generate_response, args=(rgi_queue, rgo_queue,))
    worker.setDaemon(True)
    worker.start()

    app.run(host='0.0.0.0', port=10004)


if __name__ == '__main__':
    agent = AutoDialogAgent.from_pretrained('${modelname}')
    start_http_agent(agent)
EOT
    chmod +x "$DIR/submission$i/human.py"
    i=$(($i+1))
done <$DIR/models.txt
