from typing import Callable, List, Any
import logging
import torch
from collections import OrderedDict
import dataclasses
from functools import partial
from dataclasses import dataclass, field
import transformers
from model import ModelPredictor
from utils import pull_model
import data

logger = logging.getLogger()


@dataclass
class DialogAgent:
    predictor: ModelPredictor = None
    dataset: data.DialogDataset = None
    parse_belief: Callable = field(init=False)

    def __post_init__(self):
        self.parse_belief = data.BeliefParser(self.dataset.domains)

    @classmethod
    def from_pretrained(cls, model_name, dataset=None):
        if dataset is None:
            dataset = 'multiwoz-test'
        if isinstance(dataset, str):
            dataset = data.load_dataset(dataset)

        return cls._from_pretrained(model_name, dataset)

    @staticmethod
    def _from_pretrained(model_name, dataset):
        model_name = pull_model(model_name)
        predictor = ModelPredictor.from_pretrained(model_name)
        number_of_parameters = sum(x.numel() for x in predictor.model.parameters())
        logger.info(f'model loaded, number of parameters: {number_of_parameters}')
        return DialogAgent(predictor, dataset)

    def format_results(self, results):
        add_labels = data.InsertLabelsTransformation()
        formatted = []
        for i, (bs, db, res) in enumerate(results):
            sample = add_labels(([], bs, db, res, 1))
            formatted.append('=>' + sample.belief + '<|eob|>' + sample.database +
                             '<|eokb|>' + sample.response + '<|endoftext|>')
        return formatted

    def _lexicalise(self, response, db, belief, context):
        return self.dataset.lexicalizer(response, db, belief=belief, context=context)

    def __call__(self, contexts: List[List[str]], lexicalise=True, return_strings=False):
        if self.dataset.normalize_input is not None:
            contexts = [[self.dataset.normalize_input(y) for y in x] for x in contexts]
        original_belief_strs = self.predictor.predict_belief(contexts)
        beliefs = list(map(self.parse_belief, original_belief_strs))
        dbs_results = list(map(partial(self.dataset.database, return_results=True), beliefs))
        dbs = [OrderedDict((k, x[0]) for k, x in db.items()) for db in dbs_results]
        belief_strs = list(map(data.format_belief, beliefs))  # TODO (Vojta): try this vs original belief_strs
        delex_responses = self.predictor.predict_response(contexts, belief_strs, dbs)
        results = [beliefs, dbs, delex_responses]
        if lexicalise:
            responses = [self._lexicalise(response, db, bf, ctx)
                         for response, bf, db, ctx in zip(delex_responses, beliefs, dbs_results, contexts)]
            results.append(responses)
        else:
            responses = delex_responses

        if return_strings:
            output_strings = self.format_results(zip(original_belief_strs, dbs, responses))
            results.append(output_strings)
        return list(zip(*results))

    def to(self, device):
        return dataclasses.replace(self, predictor=self.predictor.to(device))


@dataclass
class PostfixDialogAgent(DialogAgent):
    postfix_model: Any = None
    postfix_tokenizer: Any = None
    device: Any = None

    def _lexicalise(self, response, database, belief, context):
        response = super()._lexicalise(response, database, belief, context)

        # Apply the postfix model
        query = self.postfix_tokenizer.encode(response)
        response_tokens = self.postfix_model.generate(torch.tensor([query], device=self.device))[0]
        response = self.postfix_tokenizer.decode(response_tokens[2:])

        return response

    def to(self, device):
        return dataclasses.replace(super().to(device), device=device,
                                   postfix_model=self.postfix_model.to(device))

    @staticmethod
    def _from_pretrained(model_names, dataset):
        main_model, postfix_model = map(pull_model, model_names)
        predictor = ModelPredictor.from_pretrained(main_model)
        number_of_parameters = sum(x.numel() for x in predictor.model.parameters())
        logger.info(f'predictor model loaded, number of parameters: {number_of_parameters}')

        postfix_tokenizer = transformers.AutoTokenizer.from_pretrained(postfix_model)
        postfix_model = transformers.AutoModelWithLMHead.from_pretrained(postfix_model)
        number_of_parameters = sum(x.numel() for x in postfix_model.parameters())
        logger.info(f'postfix model loaded, number of parameters: {number_of_parameters}')
        return PostfixDialogAgent(predictor, dataset, postfix_model=postfix_model, postfix_tokenizer=postfix_tokenizer)


class AutoDialogAgent:
    @staticmethod
    def from_pretrained(name, dataset=None):
        if '+' in name:
            return PostfixDialogAgent.from_pretrained(name.split('+'), dataset)
        return DialogAgent.from_pretrained(name)

    @staticmethod
    def download_model(name):
        if '+' in name:
            return list(map(AutoDialogAgent.download_model, name.split('+')))
        return pull_model(name)
