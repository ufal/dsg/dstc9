import os
import functools
import logging
from data.utils import ConcatDialogDataset, split_name, wrap_dataset_with_blacklist

RESTRICTED_DOMAINS = ['hotel', 'train', 'restaurant', 'attraction', 'taxi',
                      'hospital', 'police', 'rentalcar', 'flight', 'hotels',
                      'restaurant-search', 'flights']
KNOWN_DATASETS = ['multiwoz', 'taskmaster', 'schemaguided']
logger = logging.getLogger()


def load_dataset(name, restrict_domains=False, augment='disabled', use_blacklist=False, **kwargs):
    if restrict_domains:
        return load_dataset(name, domains=RESTRICTED_DOMAINS, **kwargs)

    if '+' in name:
        # This is a concat dataset
        datasets = name.split('+')
        _load_dataset = functools.partial(load_dataset, **kwargs)
        datasets = list(map(_load_dataset, datasets))
        return ConcatDialogDataset(datasets)

    dataset_name, split = split_name(name)
    if dataset_name == 'all':
        return load_dataset('+'.join(f'{d}-{split}' for d in KNOWN_DATASETS))

    if dataset_name == 'multiwoz':
        from data.convlab_dataset import load_convlab_dataset
        dataset = load_convlab_dataset(name, **kwargs)

    else:
        from data.dataset import load_dataset as load_custom_dataset
        dataset = load_custom_dataset(name, **kwargs)

    if use_blacklist:
        dataset = add_blacklist(dataset, name)
    return dataset


def add_blacklist(dataset, name):
    from utils import pull_dataset
    dataset_name, split = split_name(name)
    blacklist_root = pull_dataset(f'{dataset_name}-blacklist:latest')
    with open(os.path.join(blacklist_root, f'{split}-blacklist.txt'), 'r') as f:
        blacklist = sorted(set(int(x.rstrip()) for x in f))
    logging.warning(f'Some examples ({100 * len(blacklist) / len(dataset):.2f}%) were ignored by a blacklist.')
    return wrap_dataset_with_blacklist(dataset, blacklist)


def load_backtranslation_transformation(name):
    import data.backtranslation

    def load_backtranslations(name):
        if '-' in name and split_name(name)[0] == 'all':
            return load_backtranslations('+'.join(f'{d}-train' for d in KNOWN_DATASETS))

        if '+' in name:
            datasets = name.split('+')
            backtranslations = dict()
            for dataset in datasets:
                backtranslations.update(load_backtranslations(dataset))
            return backtranslations

        if ':' not in name:
            name, _ = split_name(name)
            return load_backtranslations(f'{name}-backtranslations:latest')

        else:
            # This is an artifact
            import utils
            path = utils.pull_dataset(name)
            backtranslations = dict()
            for f in os.listdir(path):
                if f.endswith('.yaml'):
                    backtranslations.update(data.backtranslation.load_backtranslations(os.path.join(path, f)))
            return backtranslations

    return data.backtranslation.BackTranslateAugmentation(load_backtranslations(name))


def load_augment_transformation(name, mode):
    if mode == 'disabled':
        return lambda x: x

    assert '+' not in name
    name, split = split_name(name)
    assert name == 'multiwoz'

    from data.convlab_dataset import AugmentTransformation
    return AugmentTransformation(mode)
