######################################################################
######################################################################
#  Copyright Tsung-Hsien Wen, Cambridge Dialogue Systems Group, 2016 #
######################################################################
######################################################################
import os
import re

fin = open(os.path.join(os.path.dirname(__file__), 'mapping.pair'))
replacements = []
for line in fin.readlines():
    tok_from, tok_to = line.replace('\n', '').split('\t')
    replacements.append((' '+tok_from+' ', ' '+tok_to+' '))


def capitalize(text):
    return text[:1].upper() + text[1:]


def insert_space(token, text):
    sidx = 0
    while True:
        sidx = text.find(token, sidx)
        if sidx == -1:
            break
        if sidx+1 < len(text) and re.match('[0-9]', text[sidx-1]) and \
                re.match('[0-9]', text[sidx+1]):
            sidx += 1
            continue
        if text[sidx-1] != ' ':
            text = text[:sidx] + ' ' + text[sidx:]
            sidx += 1
        if sidx+len(token) < len(text) and text[sidx+len(token)] != ' ':
            text = text[:sidx+1] + ' ' + text[sidx+1:]
        sidx += 1
    return text


def normalize(text):

    # lower case every word
    # text = text.lower()

    # replace white spaces in front and end
    text = re.sub(r'^\s*|\s*$', '', text)

    # normalize phone number
    # ms = re.findall(r'\(?(\d{3})\)?[-.\s]?(\d{3})[-.\s]?(\d{4})', text)
    # if ms:
    #     sidx = 0
    #     for m in ms:
    #         sidx = text.find(m[0], sidx)
    #         if text[sidx-1] == '(':
    #             sidx -= 1
    #         eidx = text.find(m[-1], sidx)+len(m[-1])
    #         text = text.replace(text[sidx:eidx], ''.join(m))

    # replace st.
    text = text.replace(';', ',')
    text = re.sub(r'$\/', '', text)
    text = text.replace('/', ' and ')

    # replace other special characters
    text = re.sub(r'[\"\<>@]', '', text)
    #text = re.sub('[\"\<>@\(\)]','',text)
    text = text.replace(' - ', '')

    # insert white space before and after tokens:
    for token in ['?', '.', ',', '!']:
        text = insert_space(token, text)

    # replace it's, does't, you'd ... etc
    text = re.sub('^\'', '', text)
    text = re.sub('\'$', '', text)
    text = re.sub('\'\\s', ' ', text)
    text = re.sub('\\s\'', ' ', text)
    for fromx, tox in replacements:
        text = ' '+text+' '
        text = text.replace(fromx, tox)
        text = text.replace(capitalize(fromx), capitalize(tox))
        text = text[1:-1]

    # insert white space for 's
    text = insert_space('\'s', text)

    # remove multiple spaces
    text = re.sub(' +', ' ', text)

    # concatenate numbers
    # tokens = text.split()
    # i = 1
    # while i < len(tokens):
    #     if re.match(u'^\\d+$', tokens[i]) and \
    #             re.match(u'\\d+$', tokens[i-1]):
    #         tokens[i-1] += tokens[i]
    #         del tokens[i]
    #     else:
    #         i += 1
    # text = ' '.join(tokens)

    return text


def clear_whitespaces(text):
    text = re.sub(r'[\s\n\r]+', ' ', text)
    text = ' ' + text + ' '
    text = re.sub(r'\s([,\.:\?\!\']+)', lambda m: m.group(1), text)
    return text.strip()


def denormalize(text):
    text = text.replace(' -', '')
    text = clear_whitespaces(text)
    return text
