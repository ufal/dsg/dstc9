import copy
import re
from collections import OrderedDict, Counter
from functools import partial, reduce
from .nlp import normalize


ATTRIBUTE_IMPORTANCE = {
    'id': 10,
    'postcode': 4,
    'arrive by': 3,
    'leave at': 3,
}


def match_value(domain, label, value, true_value):
    value = value.lower()
    true_value = true_value.lower()

    if label == 'arrive by' or label == 'leave at':
        value = value.lstrip('0')
        true_value = true_value.rstrip('0')
        values = re.findall(r'\d+', value)

        if values:
            if value.endswith('pm') and values[0] != '12':
                values[0] = str(int(values[0]) + 12)
            if len(values) == 1:
                values.append('00')
            values[1] = format(int(values[1]), '02')
            value = ':'.join(values[:2])
    elif label == 'price range' and value == 'moderately priced':
        value = 'moderate'

    ws = set(normalize(value).split())
    wts = set(normalize(value).split())

    # Compute IoU
    if not ws and not wts:
        return 1
    return len(ws.intersection(wts)) / len(ws.union(wts))


def row_compatibility_score(domain, assignment, row):
    reversed_assignment = {k: v for k, v in reversed(assignment) if k in row}
    if not reversed_assignment:
        return 0  # there are no keys in the assignment

    scores = dict()
    total_score = 0
    for k, avalue in reversed_assignment.items():
        scores[k] = match_value(domain, k, avalue, row[k])
        total_score += scores[k] * ATTRIBUTE_IMPORTANCE.get(k, 1)

    return total_score, scores


def normalize_for_test(text):
    return text.replace('gbp', 'pounds') \
        .replace('GBP', 'pounds')


def sort_and_fix_results(domain, assignment, database_results):
    assignment = list(assignment)
    first_results = list()
    rest_results = list(database_results)
    gkeys = set()
    for r in database_results:
        gkeys.update(r.keys())
    assignment = [(k, v) for k, v in assignment if k in gkeys]
    reversed_assignment = {k: v for k, v in reversed(assignment)}
    while assignment and rest_results:
        cscore = partial(row_compatibility_score, domain, assignment)
        scores = list(map(cscore, rest_results))
        top_i, (best_score, scores) = max(enumerate(scores), key=lambda x: x[1][0])
        first_results.append(rest_results.pop(top_i))
        if not best_score > 0:
            break

        if scores.get('reference', 0) > 0:
            first_results[-1]['reference'] = reversed_assignment['reference']

        # Remove matched values from the assignment
        for k, val in scores.items():
            if val > 0:
                assignment.remove((k, reversed_assignment[k]))
        reversed_assignment = {k: v for k, v in reversed(assignment)}
    return first_results + rest_results


def match_results(assignment, database_results):
    output = []
    assignment = list(assignment)
    for domain, (count, results) in database_results.items():
        if count == 0:
            output.append((domain, (0, [])))
            continue

        # Compute match score and order the results
        results = sort_and_fix_results(domain, assignment, results)
        output.append((domain, (len(results), results)))
    return OrderedDict(output)
