import re
import os
import simplejson as json
import copy
from itertools import groupby
from functools import partial
from collections import defaultdict, OrderedDict, Counter
from typing import List
from .nlp import normalize, denormalize
from logging import getLogger


logger = getLogger()


def ireplace(text, old, new):
    idx = 0
    while idx < len(text):
        index_l = text.lower().find(old.lower(), idx)
        if index_l == -1:
            return text
        newtext = new(text[index_l:index_l + len(old)])
        text = text[:index_l] + newtext + text[index_l + len(old):]
        idx = index_l + len(newtext)
    return text


regex_replace_dict = OrderedDict(
    [
        ('leave at', re.compile('(?:' + '|'.join([r'(?:' + ctx + ')'
                                                  for ctx in ['leav.+from',
                                                              'leav.+at',
                                                              'leav.+after',
                                                              'leaving by',
                                                              'leav.+closest to',
                                                              'depart.+after',
                                                              'depart.+at',
                                                              'departure.+is']]) +
                                r') ((?:\d{1,2}[:]\d{2,3})|(?:\d{1,2} (?:am|pm)))', re.IGNORECASE)),
        ('arrive by', re.compile('(?:' + '|'.join([r'(?:' + ctx + ')'
                                                   for ctx in ['arriv.+between',
                                                               'arriv.+at',
                                                               'arriv.+by',
                                                               'arriv.+after',
                                                               'arriv.+before',
                                                               ' by',
                                                               'get to.+by',
                                                               'get you into.+by',
                                                               'get there.+by',
                                                               'arriv closer to',
                                                               'arriv.+closest to',
                                                               'arriv.+prior to the']]) +
                                 r') ((?:\d{1,2}[:]\d{2,3})|(?:\d{1,2} (?:am|pm)))', re.IGNORECASE)),
        ('time', re.compile(r'((?:\d{1,2}[:]\d{2,3})|(?:\d{1,2} (?:am|pm)))', re.IGNORECASE)),
        ('phone', re.compile(r'(\d{3}[\d\s]{0,13}\d{3})', re.IGNORECASE)),
        ('stars', re.compile(r'(\d)[ -]stars?', re.IGNORECASE)),
        ('price', re.compile(r'(\d{1,3}\.\d{1,2} (?:gbp|usd|eur|pounds)?)', re.IGNORECASE)),
        ('id', re.compile(r'(TR\d{4})', re.IGNORECASE)),
        ('reference', re.compile(r'(?:^|[^a-zA-Z0-9])(?=[A-Z0-9]{8}(?:[^a-zA-Z0-9]|$))([A-Z0-9]*[A-Z][A-Z0-9]*)')),
    ])

INVALID_VALS = ['ask']

# TODO: we dont have data for days
# 'day': re.compile('monday|tuesday|wednesday|thursday|friday|saturday|sunday', re.IGNORECASE)

placeholder_re = re.compile(r'\[(\s*[\w_\s]+)\s*\]')
number_re = re.compile(r'.*(\d+|one|two|three|four|five|six|seven|eight|nine|ten|eleven|twelve)\s$')


def ends_with_number(s):
    return bool(number_re.match(s))


def _pad_str(str):
    return ' ' + str + ' '


def build_replace(values):
    regex = re.compile(' (' + '|'.join(map(re.escape, values.keys())) + ') ', re.IGNORECASE)

    def _repl(callback, replace_dict, match):
        if replace_dict is None:
            replace_dict = dict()
        key = match.group(1)
        index = match.regs[1][0]
        if key in replace_dict:
            val = replace_dict[key.lower()]
        else:
            val = values[key.lower()]
        if callback is not None:
            callback(val, key, index)
        return f' [{val}] '

    return lambda x, callback=None, replace_dict=None: regex.sub(partial(_repl, callback, replace_dict), f' {x} ')[1:-1]


def augment_time(time):
    yield time
    if time.startswith('0'):
        for val in augment_time(time.lstrip('0')):
            yield '0' + val
    if ':' in time and len(time.split(':')) == 2:
        a, b = map(int, time.split(':'))
        if a == 12:
            yield f'{a}:{b:02} pm'
        elif a > 12:
            yield f'{a - 12}:{b:02} pm'
        else:
            yield f'{a - 12}:{b:02} am'


def map_database_key(key):
    if key == 'trainID':
        key = 'id'
    key = ''.join([' '+i.lower() if i.isupper()
                   else i for i in key]).lstrip(' ')
    key = key.replace('_', ' ')
    if key == 'pricerange':
        key = 'price range'
    if key == 'taxi phone' or key == 'phone':
        key = 'phone'
    if key == 'taxi colors':
        key = 'color'
    if key == 'taxi types':
        key = 'brand'
    if key == 'ref':
        key = 'reference'
    if key == 'leaveAt':
        key = 'leave at'
    if key == 'arriveBy':
        key = 'arrive by'
    if key == 'entrance fee':
        key = 'fee'
    return key


DELEX_LABEL_MAP = {
    'Price': 'price range',
    'Fee': None,  # 'fee',
    'Addr': 'address',
    'Area': 'area',
    'Stars': 'stars',
    'Department': None,  # 'department',
    'Stay': None,  # 'stay',
    'Ref': 'reference',
    'Food': 'food',
    'Type': 'type',
    'Choice': None,  # ignore
    'Phone': 'phone',
    'Ticket': 'price',
    'Day': None,  # 'day',
    'Name': 'name',
    'Car': 'car',
    'Leave': 'leave at',
    'Time': 'time',
    'Arrive': 'arrive by',
    'Post': 'postcode',
    'Depart': None,  # 'departure',
    'People': None,  # 'people',
    'Dest':  None,  # 'destination',
    'Open': None,  # ignore
    'Id': 'id',
}


ALL_ALLOWED_DELEX_KEYS = {v for v in DELEX_LABEL_MAP.values() if v is not None}


def delexicalise_spans(response, spans, allowed_slots=None):
    allowed_slots = set(allowed_slots or [])
    # First, we clear the spans
    new_spans = []
    for i, span in enumerate(spans):
        if span[1] == 'Fee' and 'vary' in span[-3]:
            pass
        elif DELEX_LABEL_MAP[span[1]] not in allowed_slots:
            pass
        else:
            new_spans.append(span)
    spans = new_spans

    delex = []
    assignment = []
    textlen = 0
    for i, original in enumerate(response.split()):
        for span in spans:
            label = DELEX_LABEL_MAP[span[1]]
            textlen += 1 + len(original)
            if label is None:
                continue  # causes this token to be added
            if label == 'time' and ('minute' in span[-3] or 'hour' in span[-3] or 'day' in span[-3]):

                label = 'duration'
            if original in {',', '.', ':'}:
                if i == span[3]:
                    delex.append(original)
                    delex.append(f'[{label}]')
                    assignment.append((label, None, textlen))
                else:
                    continue
            if i == span[3]:
                if label == 'stars' and '-star' in original:
                    number, ext = original.split('-')
                    delex.append(f'[{label}]-{ext}')
                    original = number
                    assignment.append((label, original, textlen - len(original)))
                elif label == 'area' and original == 'the':
                    delex.append('the')
                    delex.append(f'[{label}]')
                    original = None
                    assignment.append((label, original, textlen))
                elif label == 'area' and original == 'in' and span[-3].startswith('in the '):
                    delex.extend(['in', 'the'])
                    delex.append(f'[{label}]')
                    original = None
                    assignment.append((label, original, textlen))
                elif label == 'time' and original == 'a':
                    delex.append('a')
                    delex.append(f'[{label}]')
                    original = None
                    assignment.append((label, original, textlen))
                elif label == 'stay' and 'day' in original:
                    delex.append(f'[{label}]')
                    delex.append('days' if 'days' in original else 'day')
                    assignment.append((label, original, textlen - len(original)))
                elif label == 'address' and len(delex) >= 2 and delex[-1] == ',' and delex[-2] == '[address]':
                    delex.pop()
                    label, text, index = assignment[-1]
                    assignment[-1] = (label, f'{text} , {original}', index)
                else:
                    delex.append(f'[{label}]')
                    assignment.append((label, original, textlen - len(original)))
                break
            elif span[3] < i <= span[4]:
                # already added the label
                label, text, index = assignment[-1]
                if text is None:
                    text = original
                else:
                    text = f'{text} {original}'
                if i == span[4] and label == 'area' and text.endswith(' of town'):
                    delex.extend(['of', 'town'])
                    text = text[:-len(' of town')]
                if i == span[4] and label == 'time' and text.endswith(' ride'):
                    delex.append('ride')
                    text = text[:-len(' ride')]

                assignment[-1] = (label, text, index)
                break
        else:
            delex.append(original)
    return ' '.join(delex), assignment


class ConvlabLexicalizer:
    def __init__(self, path, domains: List):
        self._databases = [os.path.join(path, 'db', domain + '_db.json') for domain in domains]
        self._prepare_dict()
        self._delex_regexes = regex_replace_dict
        self._regex_cache = dict()
        self.label_regex = re.compile(r'\[([\w\d\s]+)\]')
        self._warned_using_old_delex = False

    def _get_replace(self, tokens, replace_dict=None):
        stokens = '+'.join(sorted(tokens))
        if stokens not in self._regex_cache:
            repl_dict = OrderedDict(x for x in self._db_replacements if x[1] in tokens)
            self._regex_cache[stokens] = build_replace(repl_dict)
        return partial(self._regex_cache[stokens], replace_dict=replace_dict)

    def _prepare_dict(self):
        dic = []
        dic_general = []

        def dic_append(val, slot):
            if val not in INVALID_VALS:
                dic.append((val, slot))

        # read databases
        for fn in self._databases:
            fin = open(fn, 'rt')

            db_json = json.load(fin)
            fin.close()
            base_fn = os.path.basename(fn)
            domain = base_fn.rstrip('.json').split('_')[0]
            for ent in db_json:
                if not hasattr(ent, 'items'):
                    continue
                # todo: log invalid entry
                for key, val in ent.items():
                    if val == '?' or val == 'free':
                        pass
                    elif key == 'address':
                        dic.append(
                            (normalize(val), key))
                        dic.append(
                            (normalize(val.replace(',', '')), key))
                        if "road" in val:
                            val = val.replace("road", "rd")
                            dic.append(
                                (normalize(val), 'address'))
                        elif "rd" in val:
                            val = val.replace("rd", "road")
                            dic.append(
                                (normalize(val), 'address'))
                        elif "st" in val:
                            val = val.replace("st", "street")
                            dic.append(
                                (normalize(val), 'address'))
                        elif "street" in val:
                            val = val.replace("street", "st")
                            dic.append(
                                (normalize(val), 'address'))
                    elif key == 'name':
                        dic_append(normalize(val), 'name')
                        dic_append(val[0].upper() + val[1:], 'name')
                        if "b & b" in val:
                            val = val.replace("b & b", "bed and breakfast")
                            dic_append(normalize(val), 'name')
                        elif "bed and breakfast" in val:
                            val = val.replace("bed and breakfast", "b & b")
                            dic_append(normalize(val), 'name')
                        elif "hotel" in val and 'gonville' not in val:
                            val = val.replace("hotel", "")
                            dic_append(normalize(val), 'name')
                        elif "restaurant" in val:
                            val = val.replace("restaurant", "")
                            dic_append(normalize(val), 'name')
                    elif key == 'trainID':
                        dic.append(
                            (normalize(val), 'id'))
                    elif key in ['food', 'department', 'postcode', 'phone', 'departure', 'destination']:
                        dic.append(
                            (val, key))
                    elif key == 'pricerange':
                        dic_general.append(
                            (normalize(val), 'price range'))
                    elif key == 'area':
                        dic_general.append(
                            (normalize(val), 'area')
                        )
                    elif key == 'arriveBy':
                        for val in augment_time(val):
                            dic_append(val, 'arrive by')
                    elif key == 'leaveAt':
                        for val in augment_time(val):
                            dic_append(val, 'leave at')

            if domain == 'hospital':
                dic.append(
                    (normalize('Hills Rd'), 'address'))
                dic.append((normalize('Hills Road'),
                            'address'))
                dic.append(
                    (normalize('CB20QQ'), 'postcode'))
                dic.append(('01223245151', 'phone'))
                dic.append(('1223245151', 'phone'))
                dic.append(('0122324515', 'phone'))
                dic.append((normalize('Addenbrookes Hospital'),
                            'name'))

            elif domain == 'police':
                dic.append(('Parkside , Cambridge', 'address'))
                dic.append(
                    ('CB11JG', 'postcode'))
                dic.append(('01223358966', 'phone'))
                dic.append(('1223358966', 'phone'))
                dic.append(('Parkside Police Station', 'name'))
            elif domain == 'taxi':
                for color in db_json['taxi_colors']:
                    dic.append((color, 'color'))
                for x in db_json['taxi_types']:
                    dic.append((x, 'brand'))
            dic.append(('2 G Cambridge Leisure Park Cherry Hinton Road Cherry Hinton', 'address'))
            dic.append(('Cambridge Towninfo Centre', 'agent'))

        # more general values add at the end
        dic.extend(dic_general)
        dic = [(k.lower(), v) for k, v in dic]
        dic.sort(key=lambda x: len(x[0]), reverse=True)
        self._db_replacements = dic
        self._db_dict = OrderedDict(dic)
        # by creating dict we are loosing the order which might be important but dict lookup is faster
        # dic.sort(key=lambda x: x[1])
        # all_values = {x[0] for x in dic}
        # self._db_dict = {name: [x[0] for x in vals] for name, vals in groupby(dic, key=lambda x: x[1])}
        # self._db_replace = build_replace(all_values)

    def match_results(self, assignment, database_results):
        output = []
        assignment = list(assignment)
        for domain, (count, results) in database_results.items():
            results = copy.deepcopy(results)
            if count == 0:
                output.append((domain, 0, [], 0))
                continue

            # Compute match score and order the results
            for i, row in enumerate(results):
                matched_vals = Counter()
                for key, val in assignment:
                    if matched_vals[key] == i and key in row:
                        row[key] = val
                    matched_vals[key] += 1
            rows = results
            scores = list(map(partial(self.compatibility_score, assignment), rows))
            if len(scores) > 0:
                scores, rows = zip(*sorted(zip(scores, rows), key=lambda x: x[0], reverse=True))
            output.append((domain, len(rows), rows, sum(scores)))

        # return only top 1 matched domain (so we resolve domain ambiguity)
        output = [it[:3] for it in sorted(output, key=lambda x: x[-1], reverse=True)] if len(output) > 0 else []
        output = OrderedDict((x[0], x[1:]) for x in output)
        return output

    def delexicalise(self, utt, return_replacements=False, database_results=None, belief=None, spans=None):
        database_results = self._extend_database_results(database_results, belief)

        # Delexicalise only the stuff that we can put back
        allowed_keys = {'reference'}  # always delex reference
        for domain, (count, results) in database_results.items():
            if count > 0:
                allowed_keys.update(results[0].keys())
        if 'arrive by' in allowed_keys or 'leave at' in allowed_keys:
            allowed_keys.add('time')

        if spans is not None:
            # First we use the span_info annotations
            spans = sorted(spans, key=lambda x: x[-2])
            utt, replacements = delexicalise_spans(utt, spans, allowed_keys)
            if return_replacements:
                replacements = [x[:2] for x in replacements]
                # replacements.sort(key=lambda k: k[0])
                return utt, replacements
            return utt

        else:
            # Spans are none
            # Basically MultiWOZ 2.0
            # We will use the old MultiWOZ delexicalization
            # And warn if that was intentional
            if not self._warned_using_old_delex:
                logger.warn('No span info provided, we will use MultiWOZ rule-based delexicalization')
                self._warned_using_old_delex = True

            replacements = []

            def on_replace(key, value, index):
                replacements.append((key, value, index))

            # Normalize for base replacements
            utt = normalize(utt)

            # TODO: fix data and uncomment
            # utt = self._get_replace(allowed_keys)(utt, on_replace)
            utt = self._get_replace(ALL_ALLOWED_DELEX_KEYS)(utt, on_replace)

            # New regex replacements
            for label, regex in self._delex_regexes.items():
                def replace(match):
                    assert len(match.groups()) == 1
                    val = match.group(1)
                    output = match.group(0).replace(val, f'[{label}]')
                    index = match.regs[1][0]
                    on_replace(label, val, index)
                    return output

                if label not in allowed_keys:
                    continue
                utt = regex.sub(replace, utt)

            utt = denormalize(utt)
            if return_replacements:
                replacements = [x[:2] for x in sorted(replacements, key=lambda x: x[2])]
                # replacements.sort(key=lambda k: k[0])
                return utt, replacements
            return utt

    def compatibility_score(self, assignment, row):
        # TODO: implement this
        values = set(v for _, v in assignment)
        row_values = set()
        for val in row.values():
            if isinstance(val, list):
                val = tuple(val)
            if isinstance(val, dict):
                continue
            row_values.add(val)
        values.intersection_update(row_values)
        return len(values)

    def _extend_database_results(self, database_results, belief):
        # Augment database results from the belief state
        database_results = OrderedDict(database_results)
        if belief is not None:
            for i, (domain, (num_results, results)) in enumerate(database_results.items()):
                if domain not in belief:
                    continue
                if num_results == 0:
                    database_results[domain] = (1, [belief[domain]])
                else:
                    new_results = []
                    for r in results:
                        r = dict(**r)
                        for k, val in belief[domain].items():
                            if k not in r:
                                r[k] = val
                        new_results.append(r)
                    database_results[domain] = (num_results, new_results)
        return database_results

    def __call__(self, *args, **kwargs):
        return self.lexicalise(*args, **kwargs)

    def lexicalise(self, text, database_results, belief=None, context=None):
        database_results = self._extend_database_results(database_results, belief)
        result_index = 0
        last_assignment = defaultdict(set)

        def trans(label, span, force=False, loop=100):
            nonlocal result_index
            nonlocal last_assignment
            result_str = None

            for domain, (count, results) in database_results.items():
                if count == 0:
                    continue
                result = results[result_index % len(results)]
                if label in result:
                    result_str = result[label]
                    if result_str == '?':
                        result_str = 'unknown'
                    if label == 'price range' and result_str == 'moderate' and \
                            not text[span[1]:].startswith(' price range') and \
                            not text[span[1]:].startswith(' in price'):
                        result_str = 'moderately priced'
                    if label == 'type':
                        if text[:span[0]].endswith('no ') or text[:span[0]].endswith('any ') or \
                                text[:span[0]].endswith('some ') or ends_with_number(text[:span[0]]):
                            if not result_str.endswith('s'):
                                result_str += 's'
                if label == 'time' and ('[leave at]' in text or '[arrive by]' in text) and \
                    belief is not None and 'train' in belief and \
                        any([k in belief['train'] for k in ('leave at', 'arrive by')]):
                    # this is a specific case in which additional [time] slot needs to be lexicalised
                    # directly from the belief state
                    # "The earliest train after [time] leaves at ... and arrives by ..."
                    if 'leave at' in belief['train']:
                        result_str = belief['train']['leave at']
                    else:
                        result_str = belief['train']['arrive by']
                elif force:
                    if label == 'time':
                        if 'leave at' in result or 'arrive by' in result:
                            if 'arrive' in text and 'arrive by' in result:
                                result_str = result['arrive by'].lstrip('0')
                            elif 'leave at' in result:
                                result_str = result['leave at'].lstrip('0')
                        elif context is not None and len(context) > 0:
                            last_utt = context[-1]
                            mtch = regex_replace_dict['time'].search(last_utt)
                            if mtch is not None:
                                result_str = mtch.group(0).lstrip('0')
                if result_str is not None:
                    break
            if force and result_str is None:
                if label == 'reference':
                    result_str = 'YF86GE4J'
                elif label == 'phone':
                    result_str = '01223358966'
                elif label == 'postcode':
                    result_str = 'CB11JG'
                elif label == 'agent':
                    result_str = 'Cambridge Towninfo Centre'
                elif label == 'stars':
                    result_str = '4'

            if result_str is not None and result_str.lower() in last_assignment[label] and loop > 0:
                result_index += 1
                return trans(label, force=force, loop=loop - 1, span=span)

            if result_str is not None:
                last_assignment[label].add(result_str.lower())
            return result_str or f'[{label}]'

        text = placeholder_re.sub(lambda m: trans(m.group(1), span=m.span()), text)
        text = placeholder_re.sub(lambda m: trans(m.group(1), force=True, span=m.span()), text)
        return text
