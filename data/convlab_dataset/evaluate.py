import re
import copy
from functools import partial
from tqdm import tqdm
import nltk
from collections import defaultdict, Counter
from .dataset import load_convlab_dataset
from .dataset import BeliefStateTransformation
from data.utils import BeliefParser


class BaseEvaluator(object):
    def initialize(self):
        raise NotImplementedError

    def add_example(self, ref, hyp):
        raise NotImplementedError

    def get_report(self, *args, **kwargs):
        raise NotImplementedError

    @staticmethod
    def _get_prec_recall(tp, fp, fn):
        precision = tp / (tp + fp + 10e-20)
        recall = tp / (tp + fn + 10e-20)
        f1 = 2 * precision * recall / (precision + recall + 1e-20)
        return precision, recall, f1

    @staticmethod
    def _get_tp_fp_fn(label_list, pred_list):
        tp = len([t for t in pred_list if t in label_list])
        fp = max(0, len(pred_list) - tp)
        fn = max(0, len(label_list) - tp)
        return tp, fp, fn


def get_logger():
    import logging  # noqa:F811
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    return logger


class MultiWozEvaluator(BaseEvaluator):
    def __init__(self, dataset, is_multiwoz_eval=False, logger=None):
        self.db = dataset.database
        self.dataset = dataset
        self.labels = list()
        self.hyps = list()
        self.belief_parser = BeliefParser(self.db.supported_domains)
        self.belief_transformation = BeliefStateTransformation()
        self.is_multiwoz_eval = is_multiwoz_eval
        self.logger = logger or get_logger()
        self.label_regex = re.compile(r'\[([\w\d\s]+)\]')

    def add_example(self, ref, hyp):
        self.labels.append(ref)
        self.hyps.append(hyp)

    def _fix_database_query(self, query):
        if 'type' in query and query['type'] == 'mutliple sports':
            query['type'] = 'multiple sports'
        if 'internet' in query and query['internet'] == 'no':
            del query['internet']  # no internet by default
        if 'parking' in query and query['parking'] == 'no':
            del query['parking']  # no internet by default
        return query

    def _parse_goal(self, gt_belief, goal, dialog_goal, domain):
        """Parses user goal into dictionary format."""
        if domain not in gt_belief and not self.is_multiwoz_eval:
            return goal
        goal[domain] = {}
        goal[domain] = {'informable': [], 'requestable': [], 'booking': []}
        if 'info' in dialog_goal[domain]:
            # if d['goal'][domain].has_key('info'):
            if domain == 'train':
                # we consider dialogues only where train had to be booked!
                if 'book' in dialog_goal[domain]:
                    # if d['goal'][domain].has_key('book'):
                    goal[domain]['requestable'].append('reference')
                if 'reqt' in dialog_goal[domain]:
                    # if d['goal'][domain].has_key('reqt'):
                    if 'trainID' in dialog_goal[domain]['reqt']:
                        goal[domain]['requestable'].append('id')
            else:
                if 'reqt' in dialog_goal[domain]:
                    # if d['goal'][domain].has_key('reqt'):
                    for s in dialog_goal[domain]['reqt']:  # addtional requests:
                        if s in ['phone', 'address', 'postcode', 'reference', 'id']:
                            # ones that can be easily delexicalized
                            goal[domain]['requestable'].append(s)
                if 'book' in dialog_goal[domain]:
                    # if d['goal'][domain].has_key('book'):
                    goal[domain]['requestable'].append("reference")

            goal[domain]["informable"] = dialog_goal[domain]['info']
            if 'book' in dialog_goal[domain]:
                # if d['goal'][domain].has_key('book'):
                goal[domain]["booking"] = dialog_goal[domain]['book']
            if not self.is_multiwoz_eval:
                goal[domain]['informable'] = self._fix_database_query(goal[domain]['informable'])
        belief = {domain: {'semi': goal[domain]['informable']}}
        belief = self.belief_transformation(belief, [], domain).get(domain, dict())
        for key, val in gt_belief.get(domain, dict()).items():
            if key in belief:
                belief[key] = val
        goal[domain]['informable'] = belief
        return goal

    def _query_original_db(self, domain, belief):
        belief = {domain: belief}
        return self.db(belief, return_results=True)[domain][1]

    def _is_booked(self, raw_belief, domain):
        return domain in raw_belief and 'book' in raw_belief[domain] and \
            'booked' in raw_belief[domain]['book'] and \
            any('reference' in x for x in raw_belief[domain]['book']['booked'])

    def _get_requestables_and_venues(self, beliefs, responses, raw_beliefs):
        # for computing corpus success
        requestables = {'phone', 'address', 'postcode', 'reference', 'id'}
        provided_requestables = defaultdict(lambda: set())
        venue_offered = defaultdict(lambda: [])
        for i, (belief, response) in enumerate(zip(beliefs, responses)):
            # TODO:
            #                 pred_beliefs = remove_model_mismatch_and_db_data(
            #                     dialname, target_beliefs, pred_beliefs[t], domain, t)
            database_results = self.db(belief, return_results=True)
            current_requestables = set(self.label_regex.findall(response))
            self.logger.debug(response)
            current_domain = next(iter(belief.keys())) if belief else None
            self.logger.debug(f'domain: {current_domain}, r: {current_requestables}')
            self.logger.debug(f"belief: {belief.get('hotel', None)}")
            self.logger.debug(f"db: {database_results.get('hotel', None)}")
            for domain, (num_results, results) in database_results.items():
                current_delex_requestables = set(belief.get(domain, dict()).keys())
                if num_results > 0:
                    current_delex_requestables.update(results[0].keys())

                matched_requestables = current_requestables.intersection(current_delex_requestables)
                if 'reference' in matched_requestables and domain in {'restaurant', 'hotel', 'train'}:
                    # https://github.com/budzianowski/multiwoz/blob/a24d299fafa00371d03880bce34cb3b0923518fa/evaluate.py#L248
                    # if db pointer was allowing for that
                    if not self._is_booked(raw_beliefs[i], domain):
                        matched_requestables.remove('reference')

                current_requestables -= matched_requestables
                provided_requestables[domain].update(matched_requestables.intersection(requestables))

                # Venues offered
                if domain in ['restaurant', 'hotel', 'attraction', 'train']:
                    venues = results
                    if len(venue_offered[domain]) == 0 and venues:
                        venue_offered[domain] = venues
                    else:
                        flag = False
                        for ven in venues:
                            if venue_offered[domain][0].get('id') == ven.get('id'):
                                flag = True
                                break
                        if not flag and venues:  # sometimes there are no results so sample won't work
                            venue_offered[domain] = venues
                else:
                    venue_offered[domain] = domain + '_name'

            # These slots are not lexicalised back, but its is not a concern
            # in multiwoz evaluation which does not take it into account
            if current_domain and self.is_multiwoz_eval:
                provided_requestables[current_domain].update(current_requestables)
        return provided_requestables, venue_offered

    def _evaluate_generated_dialogue(self, real_requestables, provided_requestables,
                                     venue_offered, goal, stats):
        # if name was given in the task
        for domain in goal.keys():
            # if name was provided for the user, the match is being done automatically
            # if realDialogue['goal'][domain].has_key('info'):
            if 'informable' in goal[domain]:
                # if realDialogue['goal'][domain]['info'].has_key('name'):
                if 'name' in goal[domain]['informable']:
                    venue_offered[domain] = domain + '_name'

            # special domains - entity does not need to be provided
            if domain in ['taxi', 'police', 'hospital']:
                venue_offered[domain] = domain + '_name'

            # if id was not requested but train was found we dont want
            # to override it to check if we booked the right train
            if domain == 'train' and (not venue_offered[domain] and 'id' not in goal['train']['requestable']):
                venue_offered[domain] = domain + '_name'

        """
        Given all inform and requestable slots
        we go through each domain from the user goal
        and check whether right entity was provided and
        all requestable slots were given to the user.
        The dialogue is successful if that's the case for all domains.
        """
        stat_domain_total, stat_domain_match, stat_domain_success = stats

        # MATCH
        match = 0.0
        for domain in goal.keys():
            domain_success = False
            if domain in ['restaurant', 'hotel', 'attraction', 'train']:
                goal_venues = self._query_original_db(domain, goal[domain]['informable'])

                if isinstance(venue_offered[domain], str):
                    if '_name' in venue_offered[domain]:
                        domain_success = True
                elif len(venue_offered[domain]) > 0:
                    reference = venue_offered[domain][0]['id']
                    if any(isinstance(x, dict) and x.get('id') == reference for x in goal_venues):
                        domain_success = True
            else:
                if domain + '_name' in venue_offered[domain]:
                    domain_success = True
            match += domain_success
            stat_domain_total[domain] += 1
            stat_domain_match[domain] += domain_success

        if match == len(goal.keys()):
            match = 1.0
        else:
            match = 0.0

        # SUCCESS
        success = 0.0
        if match == 1.0:
            for domain in goal.keys():
                # if values in sentences are super set of requestables
                prov_req = provided_requestables[domain].intersection(real_requestables[domain])
                domain_success = len(prov_req) == len(real_requestables[domain])
                success += domain_success
                stat_domain_success[domain] += domain_success

            if success >= len(real_requestables):
                success = 1.0
            else:
                success = 0.0

        if success == 0:
            # print((real_requestables, provided_requestables))
            pass
        return success, match

    def _get_goal_and_requestables(self, gt_belief, dialog_goal):
        domains = ['restaurant', 'hotel', 'attraction', 'train', 'taxi', 'hospital', 'police']
        # requestables = ['phone', 'address', 'postcode', 'reference', 'id']

        # get the list of domains in the goal
        goal = {}
        for domain in domains:
            if dialog_goal[domain]:
                goal = self._parse_goal(gt_belief, goal, dialog_goal, domain)

        # compute corpus success
        real_requestables = {}
        provided_requestables = {}
        venue_offered = {}
        for domain in goal.keys():
            provided_requestables[domain] = []
            venue_offered[domain] = []
            real_requestables[domain] = goal[domain]['requestable']
        return goal, real_requestables

    def _parse_entities(self, tokens):
        entities = []
        for t in tokens:
            if '[' in t and ']' in t:
                entities.append(t)
        return entities

    def pack_dialogues(self, dataset, beliefs, responses):
        def batch_dialog(dialog):
            (items, goals, beliefs, responses) = tuple(zip(*dialog))
            return items, goals[0], beliefs, responses

        if isinstance(dataset, str):
            dataset = load_convlab_dataset(dataset, goal=True, _multiwoz_raw_belief=True)
        current_dialogue = []
        for item, belief, response in zip(dataset, beliefs, responses):
            if len(item.context) == 1:
                if current_dialogue:
                    yield batch_dialog(current_dialogue)
                current_dialogue = []
            current_dialogue.append((item, item.goal, belief, response))
            # current_dialogue.append((item, item.goal, item.raw_belief, item.response))
        yield batch_dialog(current_dialogue)

    def evaluate(self, beliefs, responses, progressbar=False):
        dialogues = self.pack_dialogues(self.dataset, beliefs, responses)
        successes, matches = 0, 0
        stats = tuple(Counter() for _ in range(3))
        domain_total, domain_match, domain_success = stats
        total = 0

        offset = 0
        progress = tqdm(total=len(self.dataset),
                        desc=progressbar if isinstance(progressbar, str) else 'evaluating',
                        disable=not progressbar)
        for idx, (items, dialog_goal, beliefs, responses) in enumerate(dialogues):
            old_stats = copy.deepcopy(stats)
            goal, real_requestables = self._get_goal_and_requestables(items[-1].raw_belief, dialog_goal)
            raw_beliefs = [x._multiwoz_raw_belief for x in items]
            self.logger.debug(f'rr: {real_requestables}, g: {goal}')
            provided_requestables, venue_offered = self._get_requestables_and_venues(beliefs, responses, raw_beliefs)
            success, match = self._evaluate_generated_dialogue(
                real_requestables, provided_requestables, venue_offered, goal, stats)
            # if stats[0]['hotel'] != old_stats[0]['hotel'] and stats[1]['hotel'] != old_stats[1]['hotel'] and stats[2]['hotel'] == old_stats[2]['hotel']:
            #     assert match == 1, f'Dialog idx: {idx} has match 0, offset: {offset}, len: {len(items)}'
            #     assert success == 1, f'Dialog idx: {idx} has success 0, offset: {offset}, len: {len(items)}'
            #     pass
            successes += success
            matches += match
            total += 1
            offset += len(items)
            progress.update(len(items))

        domain_results = dict()
        for key in domain_total.keys():
            domain_results[key] = domain_match[key] / \
                float(domain_total[key]), domain_success[key] / float(domain_total[key])

        return successes / float(total), matches / float(total), domain_results


def compute_bleu_remove_reference(responses, gold_responses):
    responses = map(lambda x: x.lower(), responses)
    gold_responses = map(lambda x: x.lower(), gold_responses)
    reference_regex = re.compile(r'(?:^|[^a-zA-Z0-9])(?=[A-Z0-9]{8}(?:[^a-zA-Z0-9]|$))([A-Z0-9]*[A-Z][A-Z0-9]*|0{4}\d{4})')  # noqa:E501

    def remove_reference(x):
        return x.group(0).replace(x.group(1), 'REFERENCE')
    reference_sub = partial(reference_regex.sub, remove_reference)
    responses = map(reference_sub, responses)
    gold_responses = map(reference_sub, gold_responses)
    responses = list(map(nltk.tokenize.word_tokenize, responses))
    gold_responses = map(nltk.tokenize.word_tokenize, gold_responses)
    gold_responses = list(map(lambda x: [x], gold_responses))
    bleu = nltk.translate.bleu_score.corpus_bleu(gold_responses, responses)
    return bleu
