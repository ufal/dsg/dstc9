import importlib
from itertools import chain
import os
import re
from typing import List
from functools import partial
from collections import Counter, OrderedDict, defaultdict
import dataclasses
import logging
from copy import deepcopy

import convlab2
import convlab2.util.dataloader.dataset_dataloader
from data.utils import split_name, DialogDataset, DialogDatasetItem, format_belief
from utils import pull_dataset
from .lexicaliser import ConvlabLexicalizer, map_database_key
from .nlp import clear_whitespaces

logger = logging.getLogger('data')
SPECIAL_DB_DOMAINS = ['Hospital', 'Police']
DEFAULT_IGNORE_VALUES = ['not mentioned', 'dont care', 'don\'t care', 'dontcare', 'do n\'t care', 'none']
MW_DOMAINS = ['restaurant', 'hotel', 'attraction',
              'train', 'taxi', 'hospital', 'police']
DATASETS_PATH = os.environ.get('DATASETS_PATH', os.path.expanduser('~/datasets'))


class BeliefStateTransformation:
    def __init__(self, include_empty: bool = False, ignore_values: List[str] = None):
        if ignore_values is None:
            ignore_values = DEFAULT_IGNORE_VALUES
        self.include_empty = include_empty
        self.ignore_values = ignore_values

    def _process_domain(self, domain_bs):
        return {self._map_slot(slot): self._clear_value(val) for slot, val in domain_bs.items()
                if (self.include_empty or
                    (len(val) > 0 and val not in self.ignore_values))}

    def _map_slot(self, slot):
        if slot == 'arriveBy':
            return 'arrive by'
        if slot == 'leaveAt':
            return 'leave at'
        if slot == 'pricerange':
            slot = 'price range'
        return slot

    def _clear_value(self, value):
        value = value.replace('>', ' ')
        if value == 'el shaddia guesthouse':
            value = 'el shaddai'
        if value == 'concerthall':
            value = 'concert hall'
        if value == 'nightclub':
            value = 'night club'
        # BUG in MW2.0
        value = value.lstrip('`')
        return value

    def __call__(self, belief_state, dialogue_act, active_domain):
        clean_belief = dict()
        for domain, domain_bs in belief_state.items():
            new_domain_bs = {}
            if 'semi' in domain_bs:
                new_domain_bs.update(domain_bs['semi'])
            if 'book' in domain_bs:
                new_domain_bs.update({k: v for k, v in domain_bs['book'].items() if k != 'booked'})
            if not domain_not_empty(domain_bs, self.ignore_values):
                continue
            new_domain_bs = self._process_domain(new_domain_bs)
            if 'internet' in new_domain_bs and new_domain_bs['internet'] == 'no':
                del new_domain_bs['internet']  # no internet by default
            if 'parking' in new_domain_bs and new_domain_bs['parking'] == 'no':
                del new_domain_bs['parking']  # no parking by default
            if len(new_domain_bs) > 0:
                clean_belief[domain] = new_domain_bs

        for domain in SPECIAL_DB_DOMAINS:
            if any([da[1] == domain for da in dialogue_act]):
                clean_belief[domain.lower()] = {}

        # Sort belief
        clean_belief = {k: OrderedDict(sorted(v.items(), key=lambda x: x[0])) for k, v in clean_belief.items()}
        active_bs = None
        if active_domain is not None:
            active_domain = active_domain.lower()
            active_bs = clean_belief.pop(active_domain, None)
        items = [(active_domain, active_bs)] if active_bs is not None else []
        items += [(k, v) for k, v in sorted(clean_belief.items(), key=lambda x: x[0])]
        result = OrderedDict(items)
        return result


def build_lexicator(name):
    assert name == 'multiwoz'
    return partial(ConvlabLexicalizer, os.path.join(convlab2.DATA_ROOT, name), MW_DOMAINS)


def domain_not_empty(domain_bs, ignore_values):
    return any(len(val) > 0 and val not in ignore_values for val in domain_bs.values())


def normalize_for_db(s):
    s = ','.join(s.split(' ,'))
    s = s.replace('swimming pool', 'swimmingpool')
    s = s.replace('night club', 'nightclub')
    s = s.replace('concert hall', 'concerthall')
    return s


def translate_to_db_col(s):
    if s == 'leave at':
        return 'leaveAt'
    elif s == 'arrive by':
        return 'arriveBy'
    elif s == 'price range':
        return 'pricerange'
    else:
        return s


def capitalize(val):
    def _mk(v):
        i, v = v
        if i == 0 or v not in {'the', 'an', 'a', 'of', 'in', 'for', 'as', 'these', 'at', 'up', 'on', 'and', 'or'}:
            return v[:1].upper() + v[1:]
        else:
            return v
    return ' '.join(map(_mk, enumerate(val.split())))


price_re = re.compile(r'\d+\.\d+')


def map_database_row(domain, row, query):
    results = dict()
    for k, val in row.items():
        k2 = map_database_key(k)
        if k == 'location':
            continue
        elif k == 'post code' or k == 'postcode':
            val = val.upper()
        elif k == 'name':
            val = capitalize(val)
        elif k == 'type' and val == 'concerthall':
            val = 'concert hall'
        elif k == 'price' and domain == 'hotel' and isinstance(val, dict):
            val = val.get('single', val.get('double', next(iter(val.values()))))
            val = f'{val} pounds'
        if k2 == 'people':
            # BUG in MW2.0
            val = val.lstrip('`')
        results[k2] = val
    if 'color' in results and 'brand' in results:
        results['car'] = f"{results['color']} {results['brand']}"
    if domain == 'train' and 'price' in row and 'people' in query:
        people = int(query['people'])

        def multiply_people(m):
            price = float(m.group(0))
            price *= people
            return format(price, '.2f')
        if people != 1:
            results['price'] = price_re.sub(multiply_people, row['price'])
    return results


class DatabaseWrapper:
    def __init__(self, inner, supported_domains, ignore_values, ontology, query_transform=None):
        self.inner = inner
        self.ignore_values = ignore_values
        self.supported_domains = supported_domains
        self.query_transform = query_transform
        self._name_map = None
        self._ontology = None
        self._regexp = None

    def _build_replace_dict(self):
        if self._regexp is not None:
            return
        clear_values = {'the', 'a', 'an', 'food'}
        clear_values.update(self.ontology[('hotel', 'type')])
        clear_values.update(self.ontology[('hotel', 'price range')])
        clear_values.update(self.ontology[('hotel', 'area')])
        clear_values.update(self.ontology[('restaurant', 'type')])
        clear_values.update(self.ontology[('restaurant', 'price range')])
        clear_values.update(self.ontology[('restaurant', 'food')])
        clear_values.update(self.ontology[('restaurant', 'area')])
        clear_values.update(self.ontology[('attraction', 'type')])
        clear_values = (f' {x} ' for x in clear_values)
        self._regexp = re.compile('|'.join(map(re.escape, clear_values)))
        db_entities = chain(self.inner.dbs['attraction'], self.inner.dbs['hotel'], self.inner.dbs['restaurant'])
        self._name_map = {self._clear_name(r): r['name'].lower() for r in db_entities}

    @property
    def ontology(self):
        return self._ontology

    @ontology.setter
    def ontology(self, ontology):
        if ontology != self._ontology:
            self._regexp = None
            self._name_map = None
        self._ontology = ontology

    def _clear_name(self, domain_bs):
        self._build_replace_dict()
        name = ' ' + domain_bs['name'].lower() + ' '
        name = self._regexp.sub(' ', name)
        name = re.sub(r'\s+', ' ', name)
        name = name.strip()
        return name

    def __call__(self, belief, return_results=False):
        if self.query_transform is not None:
            belief = self.query_transform(belief)
        all_results = OrderedDict()
        for domain, domain_bs in belief.items():
            if domain_not_empty(domain_bs, self.ignore_values) or \
                    domain in [d.lower() for d in SPECIAL_DB_DOMAINS]:
                def query_single(domain_bs):
                    blocked_slots = {'people', 'booked', 'stay'}
                    if domain != 'train' and domain != 'bus':
                        blocked_slots.add('day')
                    query_bs = [(translate_to_db_col(slot), normalize_for_db(val))
                                for slot, val in domain_bs.items() if slot not in blocked_slots]
                    result = self.inner.query(
                        domain, query_bs)
                    result = [map_database_row(domain, k, domain_bs) for k in result]
                    return result
                result = query_single(domain_bs)
                if len(result) == 0 and 'name' in domain_bs and self._clear_name(domain_bs) in self._name_map:
                    domain_bs = dict(**domain_bs)
                    domain_bs['name'] = self._name_map[self._clear_name(domain_bs)]
                    result = query_single(domain_bs)

                if return_results:
                    all_results[domain] = (len(result), result)
                else:
                    all_results[domain] = len(result)
        return all_results


def build_database(name, ignore_values=None):
    if ignore_values is None:
        ignore_values = DEFAULT_IGNORE_VALUES

    def call():
        module = importlib.import_module(f'convlab2.util.{name}.dbquery')
        database = getattr(module, 'Database')()
        if name == 'multiwoz':
            database = DatabaseWrapper(
                database,
                MW_DOMAINS,
                ignore_values,
                hack_query
            )
        else:
            raise ValueError('Domain not supported')
        return database
    return call


def build_fix_belief_from_database(database_engine: DatabaseWrapper):
    _clear_dict = None

    def clear_dict():
        nonlocal _clear_dict
        if _clear_dict is None:
            # Build clear dict
            _clear_dict = dict()
            db_values = set()
            for x in database_engine.inner.dbs['attraction']:
                db_values.add(x['name'])
            _clear_dict = OrderedDict((x.replace("'", ''), x) for x in db_values)
        return _clear_dict

    def call(belief):
        # fix belief state, put back apostrophs
        for domain, bs in belief.items():
            for key, value in bs.items():
                bs[key] = clear_dict().get(value, value)
        return belief
    return call


def normalize(text):
    text = text.replace('swimmingpool', 'swimming pool')
    text = text.replace('nigthclub', 'night club')
    text = text.replace('Shanghi', 'Shanghai')
    return text


INTENT_MAP = dict(bye='goodbye', welcome='greet', offerbooked='notify_success',
                  nooffer='no_offer', book='notify_success', select='offer',
                  nobook='notify_failure', offerbook='offer_intent', reqmore='req_more')


def map_intent(intent):
    intent = intent.lower()
    intent = INTENT_MAP.get(intent, intent)
    return intent


def load_convlab_dataset(dataset_name, context_window_size: int = 15, role='sys',
                         goal=False, output_assignment=False, use_blacklist=False,
                         _multiwoz_raw_belief=False, **kwargs):
    # TODO: add caching
    dataset_name, data_key = split_name(dataset_name)
    ignore_values = DEFAULT_IGNORE_VALUES
    if dataset_name == 'multiwoz2.0':
        # Hack for ConvLab2 bad API
        _backup_data_root = convlab2.DATA_ROOT
        setattr(convlab2, 'DATA_ROOT', os.path.join(DATASETS_PATH, 'convlab-mw2.0'))
        setattr(convlab2.util.dataloader.dataset_dataloader, 'DATA_ROOT', convlab2.DATA_ROOT)

    data_loader = convlab2.util.dataloader.dataset_dataloader.MultiWOZDataloader(zh=False)
    database_engine = build_database('multiwoz')()
    lexicaliser = build_lexicator('multiwoz')()
    fix_belief_from_database = build_fix_belief_from_database(database_engine)
    ontology = defaultdict(lambda: set())
    blacklist = set()
    if use_blacklist:
        blacklist_root = pull_dataset(f'{dataset_name}-blacklist:latest')
        with open(os.path.join(blacklist_root, f'{data_key}-blacklist.txt'), 'r') as f:
            blacklist = set(x.rstrip() for x in f)

    def get_missing_domain(belief_state):
        for domain, domain_bs in belief_state.items():
            if domain not in database_engine.supported_domains:
                return domain
        return None

    belief_state_transformation = BeliefStateTransformation(ignore_values=ignore_values)
    raw_data = data_loader.load_data(
        data_key=data_key,
        role=role,
        utterance=True,
        dialog_act=True,
        context=True,
        context_dialog_act=True,
        belief_state=True,
        goal=True,
        span_info=True,
        context_window_size=context_window_size,
    )[data_key]
    dataset = []
    missing_domains = Counter()
    ignored = 0

    for i in range(len(raw_data['utterance'])):
        context = raw_data['context'][i]
        if len(context) == 1:
            # New dialog
            active_domain = None
            last_belief_domains = set()
        context = [clear_whitespaces(x) for x in context]
        belief = raw_data['belief_state'][i]
        response = raw_data['utterance'][i]
        dialog_act = raw_data['dialog_act'][i]
        user_act = raw_data['context_dialog_act'][i][-1]
        system_action = {map_intent(x[0]) for x in dialog_act} if dialog_act else None
        user_intent = {map_intent(x[0]) for x in user_act} if user_act else None
        goal = raw_data['goal'][i]
        span_info = raw_data['span_info'][i]
        if f'{i}' in blacklist:
            ignored += 1
            continue

        domain_counter = Counter({x[1].lower() for x in dialog_act}.intersection(database_engine.supported_domains))
        if domain_counter:
            active_domain = domain_counter.most_common(1)[0][0]
        belief_domains = belief.keys()
        if len(belief_domains - last_belief_domains) == 1:
            active_domain = next(iter(belief_domains - last_belief_domains))
        last_belief_domains = belief_domains

        if dataset_name == 'multiwoz2.0':
            # Fix for multiwoz 2.0
            lresponse = response.lower()
            if 'hotel' in lresponse:
                active_domain = 'hotel'
            if 'train' in lresponse or 'arrive' in lresponse or 'leave' in lresponse:
                active_domain = 'train'
            if 'attraction' in lresponse:
                active_domain = 'attraction'
            if 'police' in lresponse:
                active_domain = 'police'
            if 'restaurant' in lresponse or 'food' in lresponse:
                active_domain = 'restaurant'
            if 'hospital' in lresponse:
                active_domain = 'hospital'
            if 'taxi' in lresponse or 'car' in response.lower():
                active_domain = 'taxi'
            taxi_brands = ["toyota", "skoda", "bmw", 'honda', 'ford', 'audi', 'lexus', 'volvo', 'volkswagen', 'tesla']
            if any(t in response.lower() for t in taxi_brands):
                active_domain = 'taxi'

        multiwoz_raw_belief = belief
        belief = belief_state_transformation(belief, dialog_act, active_domain)
        belief = fix_belief_from_database(belief)
        for k, bs in belief.items():
            for k2, val in bs.items():
                ontology[(k, k2)].add(val)
        missing_domain = get_missing_domain(belief)
        if missing_domain is not None:
            missing_domains[missing_domain] += 1
            continue

        sample = DialogDatasetItem(context, raw_response=response, raw_belief=belief,
                                   user_intent=user_intent, system_action=system_action)
        if goal:
            setattr(sample, 'goal', goal)
        setattr(sample, 'active_domain', active_domain)
        setattr(sample, 'span_info', span_info)
        if _multiwoz_raw_belief:
            setattr(sample, '_multiwoz_raw_belief', multiwoz_raw_belief)
        dataset.append(sample)

    if any(x for x in missing_domains.values()):
        logger.warning('there are missing domains in the database and some samples were not loaded')
        for key, value in missing_domains.items():
            logger.warning(f'  - missing domain {key} with {value} examples')
        logger.warning('either remove the invalid samples from the dataset ' +
                       'or add domains to the database')
    if ignored:
        logger.warning(
            f'some examples ({ignored}, {100 * ignored/(len(dataset) + ignored):.1f}%) were ignored by the blacklist')
    database_engine.ontology = ontology

    def transform(x):
        context = [clear_whitespaces(y) for y in x.context]
        database_results = database_engine(x.raw_belief, return_results=True)
        raw_response = normalize(x.raw_response)
        response, assignment = lexicaliser.delexicalise(raw_response, return_replacements=True,
                                                        database_results=database_results,
                                                        belief=x.raw_belief, spans=x.span_info)

        # Strip results, keep count
        database_results = OrderedDict((domain, count) for domain, (count, results) in database_results.items())
        response = clear_whitespaces(response)
        raw_response = clear_whitespaces(raw_response)
        belief = format_belief(x.raw_belief)
        result = dataclasses.replace(x, context=context, belief=belief,
                                     database=database_results, response=response, raw_response=raw_response)
        if goal:
            setattr(result, 'goal', getattr(x, 'goal'))
        if output_assignment:
            setattr(result, 'assignment', assignment)
        if _multiwoz_raw_belief:
            setattr(result, '_multiwoz_raw_belief', x._multiwoz_raw_belief)
        setattr(result, 'active_domain', x.active_domain)
        return result

    if dataset_name == 'multiwoz2.0':
        setattr(convlab2, 'DATA_ROOT', _backup_data_root)
        setattr(convlab2.util.dataloader.dataset_dataloader, 'DATA_ROOT', convlab2.DATA_ROOT)

    return DialogDataset(dataset, database=database_engine, lexicalizer=lexicaliser,
                         domains=database_engine.supported_domains, ontology=ontology,
                         transform=transform, normalize_input=normalize)


def hack_query(belief):
    new_belief = OrderedDict()
    for domain, bs in belief.items():
        new_bs = OrderedDict()
        new_belief[domain] = new_bs
        for key, val in bs.items():
            val = bs[key]
            if domain == 'restaurant' and key == 'name' and val.lower() == 'charlie':
                val = 'charlie chan'
            if domain == 'restaurant' and key == 'name' and val.lower() == 'good luck':
                val = 'the good luck chinese food takeaway'
            # if domain == 'hotel' and key == 'name' and val.lower() == 'el shaddai guesthouse':
            #     val = 'el shaddai'
            new_bs[key] = val
    return new_belief


def multiwoz_dbquery_transform(pred_beliefs):
    # deepcopy so we dont modify the given state
    new_pred_beliefs = deepcopy(pred_beliefs)
    # if domain == 'hotel':
    # todo: should we use the target_beliefs?
    # if domain in target_beliefs[t]:
    #     if 'type' in new_pred_beliefs[domain] and 'type' in target_beliefs[t][domain]:
    #         if new_pred_beliefs[domain]['type'] != target_beliefs[t][domain]['type']:
    #             new_pred_beliefs[domain]['type'] = target_beliefs[t][domain]['type']
    #     elif 'type' in new_pred_beliefs[domain] and 'type' not in target_beliefs[t][domain]:
    #         del new_pred_beliefs[domain]['type']

    for domain, bs in new_pred_beliefs.items():
        if 'name' in bs and \
                bs['name'] == 'pizza hut fenditton':
            bs['name'] = 'pizza hut fen ditton'

        if domain == 'restaurant' and 'name' in bs and \
                bs['name'] == 'riverside brasserie':
            bs['food'] = 'modern european'

        if domain == 'restaurant' and 'name' in bs and \
                bs['name'] == 'charlie chan':
            bs['area'] = 'centre'

        if domain == 'restaurant' and 'name' in bs and \
                bs['name'] == 'saint johns chop house':
            bs['pricerange'] = 'moderate'

        if domain == 'restaurant' and 'name' in bs and \
                bs['name'] == 'pizza hut fen ditton':
            bs['pricerange'] = 'moderate'

        if domain == 'restaurant' and 'name' in bs and bs['name'] == 'cote':
            bs['pricerange'] = 'expensive'

        if domain == 'restaurant' and 'name' in bs and \
                bs['name'] == 'cambridge lodge restaurant':
            bs['food'] = 'european'

        if domain == 'restaurant' and 'name' in bs and \
                bs['name'] == 'cafe jello gallery':
            bs['food'] = 'peking restaurant'

        if domain == 'restaurant' and 'name' in bs and \
                bs['name'] == 'nandos':
            bs['food'] = 'portuguese'

        if domain == 'restaurant' and 'name' in bs and \
                bs['name'] == 'yippee noodle bar':
            bs['pricerange'] = 'moderate'

        if domain == 'restaurant' and 'name' in bs and \
                bs['name'] == 'copper kettle':
            bs['food'] = 'british'

        if domain == 'restaurant' and 'name' in bs and \
                bs['name'] in ['nirala', 'the nirala']:
            bs['food'] = 'indian'

        if domain == 'attraction' and 'name' in bs and \
                bs['name'] == 'vue cinema':
            if 'type' in bs:
                del bs['type']

        if domain == 'attraction' and 'name' in bs and \
                bs['name'] == 'funky fun house':
            bs['area'] = 'dontcare'

        if domain == 'attraction' and 'name' in bs and \
                bs['name'] == 'little seoul':
            bs['name'] = 'downing college'  # correct name in turn_belief_pred

        if domain == 'attraction' and 'name' in bs and \
                bs['name'] == 'byard art':
            bs['type'] = 'museum'  # correct name in turn_belief_pred

        if domain == 'attraction' and 'name' in bs and \
                bs['name'] == 'trinity college':
            bs['type'] = 'college'  # correct name in turn_belief_pred

        if domain == 'attraction' and 'name' in bs and \
                bs['name'] == 'cambridge university botanic gardens':
            bs['area'] = 'centre'  # correct name in turn_belief_pred

        if domain == 'hotel' and 'name' in bs and \
                bs['name'] == 'lovell lodge':
            bs['parking'] = 'yes'  # correct name in turn_belief_pred

        if domain == 'hotel' and 'name' in bs and \
                bs['name'] == 'whale of a time':
            bs['type'] = 'entertainment'  # correct name in turn_belief_pred

        if domain == 'hotel' and 'name' in bs and \
                bs['name'] == 'a and b guest house':
            bs['parking'] = 'yes'  # correct name in turn_belief_pred
    #
    # if dial_name == 'MUL0116.json' and domain == 'hotel' and 'area' in new_pred_beliefs[domain]:
    #     del new_pred_beliefs[domain]['area']
    return new_pred_beliefs
