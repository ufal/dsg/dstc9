import json
import os
from itertools import chain
import re
import random
import dataclasses
from collections import OrderedDict, defaultdict
from convlab2 import DATA_ROOT
from data.utils import DialogDatasetItem, format_belief, BeliefParser
from .nlp import normalize, denormalize

INVALID_VALS = ['ask']
DOMAINS = ['restaurant', 'hotel', 'attraction', 'train', 'taxi', 'hospital', 'police']
number_characters = [chr(x) for x in range(ord('0'), ord('9') + 1)]
alphabet = [chr(x) for x in range(ord('A'), ord('Z') + 1)]
reference_characters = number_characters + alphabet
numbers = 'zero|one|two|three|four|five|six|seven|eight|nine|ten'.split('|')


def prepare_values():
    replacements = defaultdict(lambda: [])

    def dic_append(val, slot):
        if val not in INVALID_VALS:
            replacements[slot].append(val)

    # read databases
    for domain in DOMAINS:
        fin = open(os.path.join(DATA_ROOT, 'multiwoz', 'db', domain + '_db.json'), 'rt')
        db_json = json.load(fin)
        fin.close()
        for ent in db_json:
            if not hasattr(ent, 'items'):
                continue
            # todo: log invalid entry
            for key, val in ent.items():
                if val == '?' or val == 'free':
                    pass
                elif key == 'address':
                    dic_append(normalize(val), key)
                    dic_append(normalize(val.replace(',', '')), key)
                    if "road" in val:
                        val = val.replace("road", "rd")
                        dic_append(normalize(val), 'address')
                    elif "rd" in val:
                        val = val.replace("rd", "road")
                        dic_append(normalize(val), 'address')
                    elif "st" in val:
                        val = val.replace("st", "street")
                        dic_append(normalize(val), 'address')
                    elif "street" in val:
                        val = val.replace("street", "st")
                        dic_append(normalize(val), 'address')
                elif key == 'name':
                    dic_append(normalize(val), 'name')
                    if "b & b" in val:
                        val = val.replace("b & b", "bed and breakfast")
                        dic_append(normalize(val), 'name')
                    elif "bed and breakfast" in val:
                        val = val.replace("bed and breakfast", "b & b")
                        dic_append(normalize(val), 'name')
                    elif "hotel" in val and 'gonville' not in val:
                        val = val.replace("hotel", "")
                        dic_append(normalize(val), 'name')
                    elif "restaurant" in val:
                        val = val.replace("restaurant", "")
                        dic_append(normalize(val), 'name')
                elif key in ['departure', 'destination']:
                    dic_append(val, key)
                    dic_append(normalize(val), key)

        if domain == 'hospital':
            dic_append(normalize('Hills Rd'), 'address')
            dic_append(normalize('Hills Road'), 'address')
            dic_append(normalize('Addenbrookes Hospital'), 'name')

        elif domain == 'police':
            dic_append('Parkside , Cambridge', 'address')
            dic_append('CB11JG', 'postcode')
            dic_append('Parkside Police Station', 'name')
        elif domain == 'taxi':
            for color in db_json['taxi_colors']:
                dic_append(color, 'color')
            for x in db_json['taxi_types']:
                dic_append(x, 'brand')
        dic_append('2 G Cambridge Leisure Park Cherry Hinton Road Cherry Hinton', 'address')

    domains = ['address', 'name', 'brand', 'color']
    return OrderedDict((d, replacements[d]) for d in domains)


def build_ontology_and_regexes(use_database, use_ontology):
    ontology = OrderedDict([
        ('day', ['Monday', 'Tuesday', 'Wednesday', 'Friday', 'Saturday', 'Sunday'])])
    if use_ontology:
        ontology = OrderedDict(chain(ontology.items(), [
            ('day', ['Monday', 'Tuesday', 'Wednesday', 'Friday', 'Saturday', 'Sunday']),
            ('price range', ['cheap', 'moderate', 'expensive']),
            ('food', ['malaysian', 'international', 'portugese', 'eastern european', 'modern eclectic', 'indian', 'african', 'english', 'new zealand', 'seafood', 'turkish', 'russian', 'scandinavian', 'indonesian', 'modern global', 'swiss', 'chinese', 'traditional', 'basque', 'mexican', 'korean', 'asian', 'barbeque modern european', 'gastropub', 'molecular gastronomy', 'thai and chinese', 'north indian', 'crossover', 'catalan', 'light bites', 'kosher', 'world', 'fusion', 'tuscan', 'swedish', 'venetian', 'scottish', 'middle eastern', 'european', 'christmas', 'vietnamese', 'austrian', 'hungarian', 'greek', 'afternoon tea', 'afghan', 'italian', 'jamaican chinese', 'japanese', 'romanian', 'modern american', 'italian', 'indian', 'australian', 'sri lankan', 'belgian',
                      'german', 'welsh', 'singaporean', 'lebanese', 'indian', 'british', 'jamaican', 'barbeque', 'australian|indian', 'cantonese', 'north african', 'eritrean', 'modern european', 'north american indian', 'danish', 'canapes', 'bistro', 'traditional american', 'latin american', 'american', 'sushi', 'persian', 'french', 'brazilian', 'brazilian', 'portuguese', 'caribbean', 'mediterranean', 'creative', 'spanish', 'portuguese', 'african', 'chinese', 'mexican', 'corsica', 'unusual', 'kosher', 'british', 'steakhouse', 'asian oriental', 'northern european', 'cuban', 'polish', 'south african', 'thai', 'spanish', 'polynesian', 'vegetarian', 'dojo noodle bar', 'north american', 'halal', 'panasian', 'irish', 'caribbean indian', 'the americas', 'moroccan', 'south indian']),
            ('hotel type', ['hotel', 'guesthouse', 'bed and breakfast']),
            ('attraction type', ['hiking', 'historical', 'concert hall', 'concert', 'boat', 'theater', 'theatre', 'swimming pool', 'churchills college', 'entertainment', 'cinema', 'multiple sports', 'gallery', 'special',
                                 'camboats', 'college', 'boating', 'museum', 'nightclub', 'hotel', 'night club', 'park', 'sports', 'park', 'boat', 'pool', 'museum kettles yard', 'museum', 'architecture', 'church', 'boat', 'gastropub'])
        ]))

    if use_database:
        ontology = OrderedDict(chain(prepare_values().items(), ontology.items()))
    regexes = OrderedDict([
        (k, re.compile(r'(?:^|[,:\.;\s=])(' + '|'.join(re.escape(x) for x in v) + r')(?:ly|s|)(?:$|[},:\.;\s])',
                       re.IGNORECASE))
        for k, v in ontology.items()] +
        [('reference', re.compile(r'(?:^|[^a-zA-Z0-9])(?=[A-Z0-9]{8}(?:[^a-zA-Z0-9]|$))([A-Z0-9]*[A-Z][A-Z0-9]*)')),
         ('time', re.compile(r'((?:\d{1,2}[:]\d{2,3})|(?:\d{1,2} (?:am|pm)))', re.IGNORECASE)),
         ('phone', re.compile(r'(\d{3}[\d\s]{0,13}\d{3})', re.IGNORECASE)),
         ('postcode', re.compile(r'((?:cb|pe)\d{2}\w{2})', re.IGNORECASE)),
         ('stars', re.compile(r'(\d)[ -]stars?', re.IGNORECASE)),
         ('price', re.compile(
             r'(\d{1,3}\.\d{1,2} (?:gbp|usd|eur|pound)?|\d{1,3} (?:gbp|usd|eur|pound))', re.IGNORECASE)),
         ('id', re.compile(r'(TR\d{4})', re.IGNORECASE)),
         ('number', re.compile(rf'(?:^|[,:\.;\s=])(\d+|{"|".join(numbers)})(?:$|[}},:\.;\s])', re.IGNORECASE))]
    )
    return ontology, regexes


def span_overlap(spans1, spans2):
    for (s, e) in spans1:
        for s2, e2 in spans2:
            if max(s, s2) <= min(e, e2):
                return True
    return False


class Augmenter:
    def __init__(self, mode='advanced', seed=42):
        assert mode in {'basic', 'minimal', 'disabled', 'advanced'}
        self.mode = mode
        self._rng = random.Random(seed)
        if self.mode == 'disabled':
            return

        self._belief_parser = BeliefParser(DOMAINS)
        self._ontology, self._regexes = build_ontology_and_regexes(mode == 'advanced', mode != 'minimal')

    def seed(self, seed):
        self._rng.seed(seed)

    def resample_assignment(self, assignment):
        augmenter_state = dict()
        new_assignment = []
        acc_index = 0
        for key, old_value, (mindex, index) in assignment:
            value = old_value
            if (key, old_value) not in augmenter_state:
                sampler = f'_sample_{key.replace(" ", "_")}'
                if hasattr(self, sampler):
                    value, augmenter_state = getattr(self, sampler)(value, augmenter_state)
                elif key in self._ontology:
                    value = self._rng.choice(self._ontology[key])
                if value != old_value:
                    augmenter_state[(key, old_value)] = value
            else:
                value = augmenter_state[(key, old_value)]
            new_assignment.append((key, value, (mindex, index + acc_index)))
            acc_index += len(value) - len(old_value)
        return new_assignment

    def _sample_number(self, value, state):
        new_number = None
        if value.isnumeric():
            number = int(value)
            if number >= 4:
                new_number = self._rng.randint(4, 2 * int(value))
                value = format(new_number)
        else:
            number = numbers.index(value.lower())
            if number >= 4:
                new_number = self._rng.randint(4, 10)
                value = numbers[new_number]
        if new_number is not None and new_number <= 10 and number <= 10:
            state[('number', format(number))] = format(new_number)
            state[('number', numbers[number])] = numbers[new_number]
        return value, state

    def _sample_price(self, value, state):
        price_match = re.search(r'\d+(?:\.\d{0,2}|)', value)
        price = float(price_match.group(0))

        offset = state.get('price_offset', None)
        if offset is None:
            max_price = int(10 * price) + 20
            min_price = max(-20, 1 - int(10 * price))
            if max_price < min_price:
                offset = 0
            else:
                offset = self._rng.randint(min_price, max_price) / 10
            state['price_offset'] = offset

        price = price + offset
        price_str = f'{price:g}'if price.is_integer() else f'{price:.2f}'
        value = price_str + value[price_match.end(0):]
        return value, state

    def _sample_time(self, value, state):
        time = list(map(int, re.findall(r'\d+', value)))[:2]
        if value.endswith('pm') and time[0] < 12:
            time[0] = time[0] + 12
        if len(time) == 1:
            time.append(0)
        h, m = time

        offset = state.get('time_offset', None)
        if offset is None:
            max_hour = min(3, 23 - h)
            min_hour = max(-3, 1 - h)
            if max_hour < min_hour:
                offset = 0
            elif self._rng.randrange(3):
                # use hour only offset
                offset = 60 * self._rng.randint(min_hour, max_hour)
            else:
                offset = self._rng.randint(min_hour * 60, max_hour * 60)
            state['time_offset'] = offset

        h = (h + (offset // 60)) % 24
        m = (m + (offset % 60)) % 60

        # Modify the time according to offset
        if ':' in value:
            if value.startswith('0') or not self._rng.randrange(3):
                new_value = f'{h:02}:{m:02}'
            else:
                new_value = f'{h}:{m:02}'
        else:
            timeval = f'{h}'
            if m != 0:
                timeval += f':{m:02}'
            if h >= 12:
                new_value = f'{timeval} pm'
            else:
                new_value = f'{timeval} am'
        return new_value, state

    def _sample_postcode(self, value, state):
        if self._rng.randrange(180):
            new_value = 'CB'
        else:
            new_value = 'PE'
        new_value += f'{self._rng.randrange(100):02}'
        new_value += ''.join(self._rng.choices(alphabet, k=2))
        return new_value, state

    def _sample_id(self, value, state):
        value = f'TR{self._rng.randrange(10000):04}'
        return value, state

    def _sample_stars(self, stars, state):
        stars = format(self._rng.randrange(0, 6))
        return stars, state

    def _sample_reference(self, reference, state):
        if self._rng.randrange(2):
            # Numbers only
            reference = format(self._rng.randrange(10000), '08')
        else:
            reference = ''.join(self._rng.choices(reference_characters, k=8))
        return reference, state

    def _sample_phone(self, phone, state):
        numbers = ''.join(self._rng.choices(number_characters, k=len(phone)))
        phone = re.sub(r'\d', lambda x: numbers[x.start()], phone)
        return phone, state

    def _get_sentence_assignment(self, sentence):
        spans = []
        matches = []

        for label, regex in self._regexes.items():
            for match in regex.finditer(sentence):
                assert len(match.groups()) == 1
                val = match.group(1)
                index = match.start(1)
                span = match.span(1)
                if not span_overlap(spans, [span]):
                    spans.append(span)
                    matches.append((label, val, index))
        return sorted(matches, key=lambda x: x[-1])

    def get_assignment(self, sentences):
        for i, sentence in enumerate(sentences):
            for (k, v, index) in self._get_sentence_assignment(sentence):
                yield (k, v, (i, index))

    def augment_sentences(self, sentences):
        if self.mode == 'disabled':
            return sentences

        assignment = list(self.get_assignment(sentences))
        new_assignment = self.resample_assignment(assignment)
        prev_mindex = -1
        acc_index = 0
        for (_, old, (mindex, old_index)), (_, new, _) in zip(assignment, new_assignment):
            if prev_mindex != mindex:
                acc_index = 0

            s = sentences[mindex]
            sentences[mindex] = s[:old_index + acc_index] + new + s[old_index + acc_index + len(old):]
            prev_mindex = mindex
            acc_index += len(new) - len(old)

        return sentences

    def _replace_belief_values(self, belief, values):
        new_belief = OrderedDict()
        i = 0
        for k, bs in belief.items():
            new_bs = OrderedDict()
            new_belief[k] = new_bs
            for k2 in bs.keys():
                new_bs[k2] = values[i]
                i += 1
        return new_belief

    def __call__(self, sample: DialogDatasetItem) -> DialogDatasetItem:
        if self.mode == 'disabled':
            return sample

        result_numbers, belief_values = '', ''
        belief, raw_belief = sample.belief, sample.raw_belief
        if sample.database:
            result_numbers = ','.join(str(v) for v in sample.database.values())
        if sample.belief:
            belief_values = ';'.join(v for bs in raw_belief.values() for v in bs.values())

        sentences = sample.context + [sample.response, sample.raw_response]
        sentences = list(map(normalize, sentences))
        sentences += [belief_values, result_numbers]
        sentences = self.augment_sentences(sentences)
        database_results = sample.database
        if sample.database:
            result_numbers = list(map(int, sentences[-1].split(',')))
            database_results = OrderedDict((d, n) for (d, _), n in zip(sample.database.items(), result_numbers))
        if sample.belief:
            raw_belief = self._replace_belief_values(raw_belief, sentences[-2].lower().split(';'))
            belief = format_belief(raw_belief)
        sentences = sentences[:-2]
        sentences = list(map(denormalize, sentences))
        return dataclasses.replace(sample, context=sentences[:-2], response=sentences[-2],
                                   raw_response=sentences[-1], database=database_results,
                                   raw_belief=raw_belief, belief=belief)
