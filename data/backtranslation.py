import os
import logging
import yaml
import dataclasses
import random
from data.utils import DialogDatasetItem

logger = logging.getLogger()


def load_backtranslations(path):
    with open(os.path.join(path)) as f:
        return yaml.safe_load(f)


class BackTranslateAugmentation:
    def __init__(self, dictionary, seed=42):
        self.dictionary = dictionary
        self._rng = random.Random(seed)
        self._num_errors = 0
        self._total = 0

    def _map(self, text: str) -> str:
        self._total += 1
        if text in self.dictionary:
            options = self.dictionary[text] + [text]
            return self._rng.choice(options)

        self._num_errors += 1
        logging.warning(
            'cannot backtranslate, unknown text in the dataset, missing {0:2.2f}%'.format(self._num_errors * 100 / self._total))

        # More than 10% of the translations are missing
        if self._total > 1000 and self._num_errors * 10 > self._total:
            logging.error('more than 10% of translations are missing')
            raise Exception('Backtranslation dictionary is missing')
        return text

    def __call__(self, item: DialogDatasetItem) -> DialogDatasetItem:
        context = [self._map(x) for x in item.context]
        return dataclasses.replace(item, context=context)
